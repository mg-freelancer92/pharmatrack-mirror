<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * Class Employees
 * @package App
 */
class Employees extends Authenticatable
{
    use SoftDeletes;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 
        'email',
        'mobile',
        'password', 
        'manager_id', 
        'level_id', 
        'hiring_date', 
        'leaving_date', 
        'status'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['hiring_date', 'leaving_date', 'created_at', 'updated_at', 'deleted_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function level()
    {
        return $this->belongsTo(Levels::class, 'level_id');
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeSM($query){
        return $query->where('level_id', 1);
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeAM($query){
        return $query->where('level_id', 2);
    }


    /**
     * @param $query
     * @return mixed
     */
    public function scopeMR($query){
        return $query->where('level_id', 3);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function manager()
    {
        return $this->belongsTo(Employees::class, 'manager_id');
    }

    /**
     * @param $value
     * @return string
     */
    public function getStatusAttribute($value)
    {
        if ($value) return "<span class='label label-success'>Active</span>";
        return "<span class='label label-danger'>Not Active</span>";
    }

    /**
     * @param $value
     * @return string
     */
    public function getLeavingDateAttribute($value)
    {
        if (!isset($value)) return;
    }
}
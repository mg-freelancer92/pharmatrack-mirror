<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LeaveRequests extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'mr_id',
        'reason',
        'leave_start',
        'leave_end',
        'approved'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at', 'leave_start', 'leave_end'];
    
    public function scopeApproved($query)
    {
        return $query->where('approved', 1);
    }

    public function scopeDeclined($query)
    {
        return $query->where('approved', 0);
    }

    public function scopeBetween($query, $from, $to)
    {
        return $query->whereBetween('created_at', [$from, $to])->get();
    }

    public function mr()
    {
        return $this->belongsTo(Employees::class, 'mr_id');
    }
}

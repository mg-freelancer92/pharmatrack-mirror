<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Customers extends Model
{
    use SoftDeletes;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 
        'mr_id', 
        'email', 
        'mobile',
        'clinic_phone',
        'class', 
        'working_place',
        'working_place_name', 
        'specialty',
        'address',
        'address_description',
        'working_time',
        'time_of_visit',
        'comment',
        'status'
    ];
    
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function visitClass()
    {
        return $this->belongsTo(VisitsClasses::class, 'class_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function mr()
    {
        return $this->belongsTo(Employees::class, 'mr_id');
    }

    /**
     * @param $value
     * @return string
     */
    public function getStatusAttribute($value)
    {
        if ($value) return "<span class='label label-success'>Active</span>";
        return "<span class='label label-danger'>Not Active</span>";
    }

}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class Plans extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'mr_id',
        'visit_date',
        'comment',
        'decline_reason',
        'approved'
    ];
    
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['visit_date', 'created_at', 'updated_at', 'deleted_at'];
    
    public function mr()
    {
        return $this->belongsTo(Employees::class, 'mr_id');
    }

    public function customers()
    {
        return $this->hasMany(PlansCustomers::class, 'plan_id');
    }
    
    public function scopeApproved($query)
    {
        return $query->where('approved', 1);
    }

    public function scopeDeclined($query)
    {
        return $query->where('approved', 0);    
    }
    
    public function scopeBetween($query, $from, $to)
    {
        return $query->whereBetween('created_at', [$from, $to])->get();
    }

    public function getCreatedAtAttribute($date)
    {
        return Carbon::createFromFormat('Y-m-d', $date)->format('Y-m-d');
    }

    public function getUpdatedAtAttribute($date)
    {
        return Carbon::createFromFormat('Y-m-d', $date)->format('Y-m-d');
    }
}
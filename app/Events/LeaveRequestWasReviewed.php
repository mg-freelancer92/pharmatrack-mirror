<?php

namespace App\Events;

use App\Events\Event;
use App\LeaveRequests;
use App\Repositories\LeaveRequestsRepository;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Support\Facades\View;

class LeaveRequestWasReviewed extends Event
{
    use SerializesModels;

    public $leaveRequest;
    public $statue;

    /**
     * Create a new event instance.
     *
     * LeaveRequestWasReviewed constructor.
     * @param $leaveRequest
     */
    public function __construct(LeaveRequests $leaveRequest)
    {
        $this->leaveRequest = $leaveRequest;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}

<?php

namespace App\Http\Requests\SM;

use App\Http\Requests\Request;

class EmployeesRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'email' => 'required|email|unique:employees,email,'.Request::get('id'),
            'mobile' => 'required|numeric|unique:employees,mobile,'.Request::get('id'),
            'password' => '',
            'manager_id' => 'required',
            'level_id' => 'required',
            'hiring_date' => 'required',
            'leaving_date' => '',
            'status' => 'required'
        ];
    }
}

<?php

namespace App\Http\Requests\SM;

use App\Http\Requests\Request;

class CustomersRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'  =>  'required',
            'mr_id' => 'required',
            'email' => 'required|email|unique:customers,email,'.Request::get('id'),
            'mobile' => 'required|numeric|unique:customers,mobile,'.Request::get('id'),
            'clinic_phone' => 'required|numeric|unique:customers,clinic_phone,'.Request::get('id'),
            'class' => 'required',
            'working_place' => 'required',
            'working_place_name' => 'required',
            'specialty' => 'required',
            'address' => 'required',
            'address_description' => '',
            'working_time' => 'required',
            'time_of_visit' => 'required',
            'comment' => '',
            'status' => 'required'
        ];
    }
}

<?php

namespace App\Http\Requests\MR;

use App\Http\Requests\Request;

class LeaveRequestsRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'leave_start'    =>  'required|date_format:"Y-m-d"',
            'leave_end' =>  'required|date_format:"Y-m-d"',
            'reason' => 'required',
        ];
    }
}

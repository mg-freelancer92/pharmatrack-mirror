<?php

namespace App\Http\Requests\MR;

use App\Http\Requests\Request;

class PlansRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'visit_date'    =>  'required|date_format:"Y-m-d"',
            'customers' =>  'required',
            'comment' => '',
        ];
    }
}

<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::auth();

Route::group(['namespace' => 'SM', 'prefix' => 'sm'], function () {
    Route::get('dashboard', 'DashboardController@index');
    
    Route::get('mrs/all', 'MRsController@get');
    Route::get('mrs/add', 'MRsController@create');
    Route::post('mrs/add', 'MRsController@store');
    Route::get('mrs/{id}/edit', 'MRsController@edit');
    Route::post('mrs/{id}/edit', 'MRsController@update');
    Route::post('mrs/{id}/delete', 'MRsController@destroy');

    Route::get('ams/all', 'AMsController@get');
    Route::get('ams/add', 'AMsController@create');
    Route::post('ams/add', 'AMsController@store');
    Route::get('ams/{id}/edit', 'AMsController@edit');
    Route::post('ams/{id}/edit', 'AMsController@update');
    Route::post('ams/{id}/delete', 'AMsController@destroy');

    Route::get('customers/all', 'CustomersController@get');
    Route::get('customers/add', 'CustomersController@create');
    Route::post('customers/add', 'CustomersController@store');
    Route::get('customers/{id}/edit', 'CustomersController@edit');
    Route::post('customers/{id}/edit', 'CustomersController@update');
    Route::post('customers/{id}/delete', 'CustomersController@destroy');

    Route::get('mrs/{id}/plans', 'PlansController@get');
    Route::get('mrs/{id}/plans/ajax', 'PlansController@ajaxMRPlans');
    Route::get('plans/{id}/approve', 'PlansController@approve');
    Route::get('plans/{id}/decline', 'PlansController@decline');

    Route::get('mrs/{id}/reports', 'ReportsController@get');

    Route::get('mrs/{id}/coverage', 'CoverageController@get');
    
    Route::get('mrs/{id}/sales', 'SalesController@get');


    Route::get('products/all', 'ProductsController@get');
    Route::get('products/add', 'ProductsController@create');
    Route::post('products/add', 'ProductsController@store');
    Route::get('products/{id}/edit', 'ProductsController@edit');
    Route::post('products/{id}/edit', 'ProductsController@update');
    Route::post('products/{id}/delete', 'ProductsController@destroy');

    Route::get('areas/all', 'AreasController@get');
    Route::get('areas/add', 'AreasController@create');
    Route::post('areas/add', 'AreasController@store');
    Route::get('areas/{id}/edit', 'AreasController@edit');
    Route::post('areas/{id}/edit', 'AreasController@update');
    Route::post('areas/{id}/delete', 'AreasController@destroy');

    Route::get('target/set', 'ProductsController@setTarget');
    Route::post('target/set', 'ProductsController@doSetTarget');

    Route::get('target/search', 'ProductsController@targetSearch');
    Route::post('target/search', 'ProductsController@doTargetSearch');

    Route::get('coverage/search', 'CoverageController@coverageSearch');
    Route::post('coverage/search', 'CoverageController@doCoverageSearch');

    Route::get('reports/search', 'ReportsController@reportSearch');
    Route::post('reports/search', 'ReportsController@doReportSearch');

    Route::get('plans/search', 'PlansController@planSearch');
    Route::post('plans/search', 'PlansController@doPlanSearch');

    Route::get('leave-requests', 'LeaveRequestsController@all');
    Route::get('leave-requests/approve/{id}', 'LeaveRequestsController@approve');
    Route::post('leave-requests/decline/{id}', 'LeaveRequestsController@decline');
});

Route::group(['namespace' => 'MR', 'prefix' => 'mr'], function () {
    Route::get('dashboard', 'DashboardController@index');
    Route::get('managers', 'ManagersController@index');
    Route::get('customers', 'CustomersController@index');

    Route::get('plans', 'PlansController@get');
    Route::get('plans/ajax', 'PlansController@ajaxPlans');
    Route::get('plans/create', 'PlansController@create');
    Route::post('plans/create', 'PlansController@store');
    Route::get('visited/{mr_id}/{customer_id}/{plan_id}', 'PlansController@isDoctorVisited');

    Route::get('leave-requests', 'LeaveRequestsController@get');
    Route::get('leave-requests/ajax', 'LeaveRequestsController@ajaxLeaveRequests');
    Route::get('leave-requests/create', 'LeaveRequestsController@create');
    Route::post('leave-requests/create', 'LeaveRequestsController@store');

    Route::get('reports', 'ReportsController@get');
    Route::get('reports/create', 'ReportsController@create');
    Route::post('reports/create', 'ReportsController@store');

    Route::get('products', 'ProductsController@get');

    Route::get('areas', 'AreasController@get');

    Route::get('target/search', 'ProductsController@targetSearch');
    Route::post('target/search', 'ProductsController@doTargetSearch');

    Route::get('coverage/search', 'CoverageController@coverageSearch');
    Route::post('coverage/search', 'CoverageController@doCoverageSearch');

    Route::get('reports/search', 'ReportsController@reportSearch');
    Route::post('reports/search', 'ReportsController@doReportSearch');

    Route::get('plans/search', 'PlansController@planSearch');
    Route::post('plans/search', 'PlansController@doPlanSearch');

    Route::get('leave-requests/search', 'LeaveRequestsController@leaveRequestSearch');
    Route::post('leave-requests/search', 'LeaveRequestsController@doLeaveRequestSearch');

    Route::get('history/search', 'DashboardController@getMonthlyHistory');
    Route::post('history/search', 'DashboardController@postMonthlyHistory');
});
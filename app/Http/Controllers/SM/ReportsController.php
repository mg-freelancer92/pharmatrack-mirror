<?php

namespace App\Http\Controllers\SM;

use App\Http\Controllers\Controller;
use App\Http\Requests\SM\ReportsSearchRequest;
use App\Repositories\EmployeesRepository;
use App\Repositories\ProductsRepository;
use App\Repositories\ReportsRepository;
use Illuminate\Support\Facades\Auth;
use App\ReportProducts;

class ReportsController extends Controller
{
    public $report;
    public $employee;
    public $product;
    const VIEW_PATH = 'sm.employees.mrs.';
    /**
     * ReportsController constructor.
     * @param ReportsRepository $report
     * @param EmployeesRepository $employee
     * @param ProductsRepository $product
     */
    public function __construct(ReportsRepository $report,
                                EmployeesRepository $employee,
                                ProductsRepository $product)
    {
        $this->report = $report;
        $this->employee = $employee;
        $this->product = $product;
    }

    public function get($MRId)
    {
        $MR = $this->employee->single($MRId);
        $reports = $this->report->getMRReports($MRId);
        return view(self::VIEW_PATH.'reports.all', compact('MR', 'reports'));
    }

    public function reportSearch()
    {
        $SMId = Auth::user()->id;
        $MRs = $this->employee->all('MR');
        $customers = $this->employee->getSMCustomers($SMId);
        $products = $this->product->all();
        return view('sm.search.reports', compact('MRs', 'customers', 'products'));
    }

    public function doReportSearch(ReportsSearchRequest $request)
    {
        $reports = [];
        if(!empty($request->mrs)) {
            foreach ($request->mrs as $mr) {
                $reports = $this->report->search([
                    ['created_at', '>=', $request->from],
                    ['created_at', '<=', $request->to],
                    ['mr_id', '=', $mr],
                    ['customer_id', $request->customer],
                    $request->has_follow_up == 1 ? ['follow_up', '<>', NULL] : ['follow_up', NULL],
                    'products' => $request->products
                ]);
            }
        }

        return view('sm.search.reports_results', compact('reports'));
    }
}

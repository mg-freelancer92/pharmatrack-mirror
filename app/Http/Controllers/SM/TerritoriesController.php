<?php

namespace App\Http\Controllers\SM;


use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Repositories\TerritoriesRepository;

class TerritoriesController extends Controller
{
    public $territory;

    /**
     * TerritoriesController constructor.
     * @param $territory
     */
    public function __construct(TerritoriesRepository $territory)
    {
        $this->territory = $territory;
    }


}

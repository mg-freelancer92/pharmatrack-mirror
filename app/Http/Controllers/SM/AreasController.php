<?php

namespace App\Http\Controllers\SM;

use App\Http\Requests\SM\AreasRequest;
use App\Repositories\AreasRepository;
use App\Http\Controllers\Controller;

class AreasController extends Controller
{
    public $area;
    const VIEW_PATH = 'sm.areas.';

    /**
     * AreasController constructor.
     * @param AreasRepository $area
     */
    public function __construct(AreasRepository $area)
    {
        $this->area = $area;
    }

    public function get()
    {
        $areas = $this->area->all();
        return view(self::VIEW_PATH.'all', compact('areas'));
    }

    public function create(){
        return view(self::VIEW_PATH.'add');
    }

    public function store(AreasRequest $request)
    {
        try{
            $this->area->store($request->all());
            return redirect()->back()->with('message','Area has been created successfully !');
        } catch (\Exception $e) {
            return $e;
        }
    }

    public function edit($id)
    {
        $area = $this->area->single($id);
        return view(self::VIEW_PATH.'edit', compact('area'));
    }

    public function update($id, AreasRequest $request)
    {
        try{
            $this->area->update($id, $request->all());
            return redirect()->back()->with('message','Area has been updated successfully !');
        } catch (\Exception $e) {
            return $e;
        }
    }

    public function destroy($id)
    {
        try{
            $this->area->destroy($id);
            return redirect()->back()->with('message','Area has been deleted successfully !');
        } catch (\Exception $e) {
            return $e;
        }
    }
}

<?php

namespace App\Http\Controllers\SM;

use App\Http\Requests\SM\PlansSearchRequest;
use App\Repositories\EmployeesRepository;
use App\Repositories\PlansRepository;
use App\Http\Controllers\Controller;

class PlansController extends Controller
{
    public $plan;
    public $employee;
    const VIEW_PATH = 'sm.employees.mrs.';

    /**
     * PlansController constructor.
     * @param PlansRepository $plan
     * @param EmployeesRepository $employee
     */
    public function __construct(PlansRepository $plan, EmployeesRepository $employee)
    {
        $this->plan = $plan;
        $this->employee = $employee;
    }

    public function get($MRId)
    {
        $MR = $this->employee->single($MRId);
        return view(self::VIEW_PATH.'plans.all', compact('MR'));
    }
    
    public function ajaxMRPlans($MRId)
    {
        $plans = array();
        $i = 0;
        foreach ($this->plan->getMRPlans($MRId) as $plan) {
            $plans[$i]['start']     =   $plan->visit_date->format('Y-m-d');
            $plans[$i]['title']     =   ($plan->comment) ? $plan->comment : '';
            $plans[$i]['color']     =   'blue';
            $i++;
            foreach ($plan->customers as $customerInPlan) {
                $plans[$i]['url']   = '';
                $plans[$i]['title'] = $customerInPlan->customer->name;
                $plans[$i]['start'] = $plan->visit_date->format('Y-m-d');
                $plans[$i]['color'] = $this->plan->isDoctorVisited($MRId, $customerInPlan->customer->id, $plan->id) == "true"
                                        ? 'green':'red';
                $i++;
            }
        }
        return json_encode($plans);
    }


    public function approve()
    {
        // TODO
    }

    public function decline()
    {
        // TODO
    }

    public function planSearch()
    {
        $MRs = $this->employee->all('MR');
        return view('sm.search.plans', compact('MRs'));
    }

    public function doPlanSearch(PlansSearchRequest $request)
    {
        $plans = [];
        if(!empty($request->mrs)) {
            foreach ($request->mrs as $mr) {
                $results = $this->plan->search([
                    ['visit_date', '>=', $request->from],
                    ['visit_date', '<=', $request->to],
                    !empty($mr) ? ['mr_id', '=', $mr] : NULL
                ]);

                foreach ($results as $key=>$plan){
                    $plans[$key]['id'] = $plan->id;
                    $plans[$key]['mr'] = $plan->mr->name;
                    $plans[$key]['visit_date'] = $plan->visit_date;
                    $plans[$key]['customers'] = $this->plan->getPlanCustomers($plan->id);
                }
            }
        }

        return view('sm.search.plans_results', compact('plans'));
    }
}
<?php

namespace App\Http\Controllers\SM;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    public function index()
    {
        return view('sm/dashboard');
    }
}

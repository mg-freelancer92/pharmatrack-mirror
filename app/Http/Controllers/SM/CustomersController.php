<?php

namespace App\Http\Controllers\SM;

use App\Http\Requests\SM\CustomersRequest;
use App\Repositories\CustomersRepository;
use App\Repositories\EmployeesRepository;
use App\Http\Controllers\Controller;

class CustomersController extends Controller
{
    public $customer;
    public $mr;
    const VIEW_PATH = 'sm.employees.';

    /**
     * CustomersController constructor.
     * @param CustomersRepository $customer
     * @param EmployeesRepository $mr
     */
    public function __construct(CustomersRepository $customer, EmployeesRepository $mr)
    {
        $this->customer = $customer;
        $this->mr = $mr;
    }
    
    public function get()
    {
        $customers = $this->customer->all();
        return view(self::VIEW_PATH.'customers.all', compact('customers'));
    }

    public function create(){
        $MRs = $this->getMRs();
        return view(self::VIEW_PATH.'.customers.add', compact('MRs'));
    }

    public function store(CustomersRequest $request)
    {
        try{
            $this->customer->store($request->except('_token'));
            return redirect()->back()->with('message','Customer has been added successfully !');
        } catch (\Exception $e) {
            return $e;
        }
    }

    public function edit($id)
    {
        $MRs = $this->getMRs();
        $customer = $this->customer->single($id);
        return view(self::VIEW_PATH.'customers.edit', compact('customer', 'MRs'));
    }

    public function update($id, CustomersRequest $request)
    {
        try{
            $this->customer->update($id, $request->except('_token'));
            return redirect()->back()->with('message','Customer has been updated successfully !');
        } catch (\Exception $e) {
            return $e;
        }
    }
    
    public function destroy($id)
    {
        try{
            $this->customer->destroy($id);
            return redirect()->back()->with('message','Customer has been deleted successfully !');
        } catch (\Exception $e) {
            return $e;
        }
    }

    /**
     * @return mixed
     */
    public function getMRs()
    {
        $MRs = $this->mr->all('MR');
        return $MRs;
    }
}

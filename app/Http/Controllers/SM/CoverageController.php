<?php

namespace App\Http\Controllers\SM;

use App\Http\Requests\SM\CoverageSearchRequest;
use App\Repositories\CoverageRepository;
use App\Repositories\EmployeesRepository;
use App\Http\Controllers\Controller;
use App\Repositories\ReportsRepository;
use Carbon\Carbon;

class CoverageController extends Controller
{
    public $coverage;
    public $report;
    public $employee;
    public $customer;

    const VIEW_PATH = 'sm.employees.mrs.';
    /**
     * CoverageController constructor.
     * @param CoverageRepository $coverage
     * @param ReportsRepository $report
     * @param EmployeesRepository $employee
     */
    public function __construct(CoverageRepository $coverage,
                                ReportsRepository $report,
                                EmployeesRepository $employee)
    {
        $this->coverage = $coverage;
        $this->report = $report;
        $this->employee = $employee;
    }
    
    public function get($MRId, $start=null, $end=null){
        $coverageForCustomers = array();
        /**
         * Get Start and End of current month
         */
        $start = Carbon::now()->startOfMonth()->format('Y-m-d');
        $end = Carbon::now()->endOfMonth()->format('Y-m-d');

        /**
         * Calculate Actual/All Coverage for Medical Rep
         */
        $actualCoverage = count($this->report->getMRReportsBetween($MRId, $start, $end));
        $allCoverage = $this->coverage->getMRCoverage($MRId);
        $coveragePercent = floatval(number_format(($actualCoverage / $allCoverage) * 100, 2));
        $MR = $this->employee->single($MRId);

        /**
         * Get All Customers for Medical
         */
        $customers = $this->employee->getMRCustomers($MRId);
        
        foreach ($customers as $customer){
            $coverageForCustomers[$customer->id]['actual_visits'] = 
                $this->report->getMRReportsForCustomer($MRId, $customer->id, $start, $end);
            
            $coverageForCustomers[$customer->id]['total_visits'] = 
                $this->coverage->getMRCoverageForCustomer($MRId, $customer->id);
        }
        return view(self::VIEW_PATH.'coverage.all', 
            compact('coveragePercent', 'MR', 'customers', 
                'start', 'end', 'actualCoverage', 'allCoverage', 'coverageForCustomers'));
    }


    public function coverageSearch()
    {
        $MRs = $this->employee->all('MR');
        return view('sm.search.coverage', compact('MRs'));
    }

    public function doCoverageSearch(CoverageSearchRequest $request)
    {
        $coverage = [];
        if(!empty($request->mrs)) {
            foreach ($request->mrs as $key=>$mr) {

                $actualCoverage = count($this->report->getMRReportsBetween($mr, $request->from, $request->to));
                $allCoverage = $this->coverage->getMRCoverage($mr);

                $coveragePercent = $allCoverage != 0
                    ? floatval(number_format(($actualCoverage / $allCoverage) * 100, 2)) : 0;

                $coverage[$key]['mr'] = $MR = $this->employee->single($mr)->name;
                $coverage[$key]['actual'] = $actualCoverage;
                $coverage[$key]['all'] = $allCoverage;
                $coverage[$key]['coverage_percent'] = $coveragePercent;
            }
        }
        return view('sm.search.coverage_results', compact('coverage'));
    }
}

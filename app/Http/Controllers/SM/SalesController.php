<?php

namespace App\Http\Controllers\SM;

use App\Repositories\EmployeesRepository;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Repositories\ManageableReportsRepository;
use App\Repositories\SalesRepository;
use Carbon\Carbon;

class SalesController extends Controller
{
    public $sales;
    public $report;
    public $mr;
    public $customer;

    const VIEW_PATH = 'sm.employees.mrs.';

    /**
     * SalesController constructor.
     * @param SalesRepository $sales
     * @param ManageableReportsRepository $report
     * @param EmployeesRepository $mr
     */
    public function __construct(SalesRepository $sales,
                                ManageableReportsRepository $report,
                                EmployeesRepository $mr)
    {
        $this->sales = $sales;
        $this->report = $report;
        $this->mr = $mr;
    }
    
    public function get($MRId, $start=null, $end=null){
        $allTargets = 0;
        $allActualSales = 0;
        
        /**
         * Get Start and End of current month
         */
        $currentMonth = lcfirst(date('M'));
        $start = Carbon::now()->startOfMonth()->format('Y-m-d');
        $end = Carbon::now()->endOfMonth()->format('Y-m-d');

        /**
         * Get Medical Rep Target
         */
        $targets = $this->sales->getMRTarget($MRId);

        /**
         * Calculate target for each product within current month
         */
        foreach($targets as $target)
        {
            $productId = $target->product_id ;
            $allActualSales +=$target->actual_sales = $this->sales->getMRSales($MRId, $productId, $start, $end);
            $allTargets += $target->$currentMonth; 
            $target->sales_percent = ($target->actual_sales / $target->$currentMonth) * 100;
        }

        /**
         * Calculate Sales Percent for all products
         */
        $allSalesPercent =  floatval(number_format($allActualSales / $allTargets * 100, 2));
        
        $MR = $this->mr->single($MRId);
        return view(self::VIEW_PATH.'sales.all', compact('targets', 'MR', 
                                                         'start', 'end', 'currentMonth', 'allSalesPercent'));
    }
}

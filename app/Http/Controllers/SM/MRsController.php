<?php
namespace App\Http\Controllers\SM;

use App\Http\Requests\SM\EmployeesRequest;
use App\Http\Controllers\Controller;
use App\Repositories\EmployeesRepository;

class MRsController extends Controller
{
    public $employee;
    const VIEW_PATH = 'sm.employees.';
    /**
     * MRsController constructor.
     * @param EmployeesRepository $employee
     */
    public function __construct(EmployeesRepository $employee)
    {
        $this->employee = $employee;
    }
    
    public function get()
    {
        $MRs = $this->employee->all('MR');
        return view(self::VIEW_PATH.'mrs.all', compact('MRs'));
    }

    public function create(){
        $AMs = $this->getAM();
        return view(self::VIEW_PATH.'mrs.add', compact('AMs'));
    }

    public function store(EmployeesRequest $request)
    {
        try{
            $this->employee->store($request->except('_token'));
            return redirect()->back()->with('message','Medical Rep has been added successfully !');
        } catch (\Exception $e) {
            return $e;
        }
    }

    public function edit($id)
    {
        $MR = $this->employee->single($id);
        $AMs = $this->getAM();
        return view(self::VIEW_PATH.'mrs.edit', compact('MR', 'AMs'));
    }

    public function update($id, EmployeesRequest $request)
    {
        try{
            $this->employee->update($id, $request->except('_token'));
            return redirect()->back()->with('message','Medical Rep has been updated successfully !');
        } catch (\Exception $e) {
            return $e;
        }
    }

    public function destroy($id)
    {
        try{
            $this->employee->destroy($id);
            return redirect()->back()->with('message','Medical Rep has been deleted successfully !');
        } catch (\Exception $e) {
            return $e;
        }
    }
    
    public function search()
    {
        return view('view');
    }
    
    public function doSearch($attributes)
    {
        try{
            return $this->employee->search($attributes);
        } catch (\Exception $e) {
            return $e;
        }
    }

    /**
     * @return mixed
     */
    public function getAM()
    {
        $AMs = $this->employee->all('AM');
        return $AMs;
    }
}
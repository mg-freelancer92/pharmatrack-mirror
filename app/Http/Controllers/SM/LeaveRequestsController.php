<?php

namespace App\Http\Controllers\SM;

use App\Events\LeaveRequestWasReviewed;
use App\LeaveRequests;
use App\Repositories\EmployeesRepository;
use App\Repositories\LeaveRequestsRepository;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Event;

class LeaveRequestsController extends Controller
{
    public $leaveRequest;
    public $employee;

    /**
     * LeaveRequestsController constructor.
     * @param $leaveRequest
     * @param $employee
     */
    public function __construct(LeaveRequestsRepository $leaveRequest, EmployeesRepository $employee)
    {
        $this->leaveRequest = $leaveRequest;
        $this->employee = $employee;
    }

    public function all()
    {
        $SMId = Auth::user()->id;
        $MRsID = $this->employee->getSMMRs($SMId, ['id'])->toArray();
        $leaveRequests= $this->leaveRequest->getMRsLeaveRequests($MRsID);
        return view('sm.employees.mrs.leave_requests.all', compact('leaveRequests'));
    }

    public function approve($id)
    {
        try {
            $this->leaveRequest->approve($id);
            Event::fire(new LeaveRequestWasReviewed(LeaveRequests::findOrFail($id)));

            return redirect()->back()->with('message','Leave Request has been approved successfully !')
                ->with('class', 'success');
        } catch (\Exception $e) {
            return $e;
        }
    }

    public function decline($id, Request $request)
    {
        try {
            $this->leaveRequest->decline($id, $request->reason);
            Event::fire(new LeaveRequestWasReviewed(LeaveRequests::findOrFail($id)));

            return redirect()->back()->with('message','Leave Request has been decline successfully !')
                ->with('class', 'danger');
        } catch (\Exception $e) {
            return $e;
        }
    }
}
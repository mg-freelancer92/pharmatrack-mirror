<?php
namespace App\Http\Controllers\SM;

use App\Http\Requests\SM\EmployeesRequest;
use App\Http\Controllers\Controller;
use App\Repositories\EmployeesRepository;

class AMsController extends Controller
{
    public $employee;
    const VIEW_PATH = 'sm.employees.';
    /**
     * AMsController constructor.
     * @param EmployeesRepository $employee
     */
    public function __construct(EmployeesRepository $employee)
    {
        $this->employee = $employee;
    }
    
    public function get()
    {
        $AMs = $this->employee->all('AM');
        return view(self::VIEW_PATH.'ams.all', compact('AMs'));
    }

    public function create(){
        return view(self::VIEW_PATH.'ams.add');
    }

    public function store(EmployeesRequest $request)
    {
        try{
            $this->employee->store($request->all());
            return redirect()->back()->with('message','Area Manager has been added successfully !');
        } catch (\Exception $e) {
            return $e;
        }
    }

    public function edit($id)
    {
        $AM = $this->employee->single($id);
        return view(self::VIEW_PATH.'ams.edit', compact('AM'));
    }

    public function update($id, EmployeesRequest $request)
    {
        try{
            $this->employee->update($id, $request->except('_token'));
            return redirect()->back()->with('message','Area Manager has been updated successfully !');
        } catch (\Exception $e) {
            return $e;
        }
    }

    public function destroy($id)
    {
        try{
            $this->employee->destroy($id);
            return redirect()->back()->with('message','Area Manager has been deleted successfully !');
        } catch (\Exception $e) {
            return $e;
        }
    }
    
    public function search()
    {
        return view('view');
    }
    
    public function doSearch($attributes)
    {
        try{
            return $this->employee->search($attributes);
        } catch (\Exception $e) {
            return $e;
        }
    }
}
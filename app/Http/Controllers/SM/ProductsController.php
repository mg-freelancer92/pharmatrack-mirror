<?php

namespace App\Http\Controllers\SM;

use App\Http\Requests\SM\ProductsRequest;
use App\Http\Requests\SM\TargetRequest;
use App\Http\Requests\SM\TargetSearchRequest;
use App\Repositories\EmployeesRepository;
use App\Repositories\ProductsRepository;
use App\Http\Controllers\Controller;

class ProductsController extends Controller
{
    public $product;
    public $employee;
    const VIEW_PATH = 'sm.products.';

    /**
     * ProductsController constructor.
     * @param ProductsRepository $product
     * @param EmployeesRepository $employee
     */
    public function __construct(ProductsRepository $product, EmployeesRepository $employee)
    {
        $this->product = $product;
        $this->employee = $employee;
    }

    public function get()
    {
        $products = $this->product->all();
        return view(self::VIEW_PATH.'all', compact('products'));
    }

    public function create(){
        return view(self::VIEW_PATH.'add');
    }

    public function store(ProductsRequest $request)
    {
        try{
            $this->product->store($request->all());
            return redirect()->back()->with('message','Product has been created successfully !');
        } catch (\Exception $e) {
            return $e;
        }
    }

    public function edit($id)
    {
        $product = $this->product->single($id);
        return view(self::VIEW_PATH.'edit', compact('product'));
    }

    public function update($id, ProductsRequest $request)
    {
        try{
            $this->product->update($id, $request->all());
            return redirect()->back()->with('message','Product has been updated successfully !');
        } catch (\Exception $e) {
            return $e;
        }
    }

    public function destroy($id)
    {
        try{
            $this->product->destroy($id);
            return redirect()->back()->with('message','Product has been deleted successfully !');
        } catch (\Exception $e) {
            return $e;
        }
    }


    public function setTarget()
    {

    }

    public function doSetTarget()
    {

    }

    public function targetSearch()
    {
        $MRs = $this->employee->all('MR');
        $products = $this->product->all();
        return view('sm.search.target', compact('MRs', 'products'));
    }

    public function doTargetSearch(TargetSearchRequest $request)
    {

    }
}

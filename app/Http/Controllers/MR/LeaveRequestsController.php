<?php

namespace App\Http\Controllers\MR;

use App\Http\Controllers\Controller;
use App\Http\Requests\MR\LeaveRequestsRequest;
use App\Http\Requests\MR\LeaveRequestsSearchRequest;
use App\Repositories\EmployeesRepository;
use App\Repositories\LeaveRequestsRepository;
use Illuminate\Support\Facades\Auth;

class LeaveRequestsController extends Controller
{
    public $leaveRequest;
    public $mr;
    const VIEW_PATH = 'mr.leave_requests.';

    /**
     * LeaveRequestsController constructor.
     * @param LeaveRequestsRepository $leaveRequests
     * @param EmployeesRepository $mr
     */
    public function __construct(LeaveRequestsRepository $leaveRequests, EmployeesRepository $mr)
    {
        $this->leaveRequest = $leaveRequests;
        $this->mr = $mr;
    }

    public function get()
    {
        $mrId = $this->getMrID();
        $leaveRequests = $this->leaveRequest->getMRLeaveRequests($mrId);
        return view(self::VIEW_PATH.'all', compact('leaveRequests'));
    }

    public function ajaxLeaveRequests()
    {
        $leaveRequests = array();
        $i = 0;
        $mrId = $this->getMrID();

        foreach ($this->leaveRequest->getMRLeaveRequests($mrId) as $leaveRequest) {
            $leaveRequests[$i]['start']         =   $leaveRequest->leave_start->format('Y-m-d');
            $leaveRequests[$i]['end']           =   $leaveRequest->leave_end->format('Y-m-d').'T23:59:59';
            $leaveRequests[$i]['title']         =   'Holiday';
            $leaveRequests[$i]['description']   =   'Reason: '. $leaveRequest->reason;
            $leaveRequests[$i]['color']         =   'black';
            $i++;
        }
        return json_encode($leaveRequests);
    }

    public function create()
    {
        return view(self::VIEW_PATH.'add', compact('customers'));
    }

    public function store(LeaveRequestsRequest $request)
    {
        try{
            $this->leaveRequest->create($request);
            return redirect()->back()->with('message','Your Leave Request waiting for approval !');
        } catch (\Exception $e) {
            return $e;
        }
    }

    public function getMrID()
    {
        return Auth::user()->id;
    }

    public function leaveRequestSearch()
    {
        return view('mr.search.leave_requests');
    }

    public function doLeaveRequestSearch(LeaveRequestsSearchRequest $request)
    {
        $leaveRequests = [];
        $MRId = Auth::user()->id;

        $leaveRequests = $this->leaveRequest->search([
            ['leave_start', '>=', $request->leave_start],
            ['leave_end', '<=', $request->leave_end],
            ['mr_id', '=', $MRId]
        ]);

        return view('mr.search.leave_requests_results', compact('leaveRequests'));
    }
}

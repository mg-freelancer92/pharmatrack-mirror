<?php

namespace App\Http\Controllers\MR;

use App\Http\Requests\MR\HistorySearchRequest;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Repositories\CoverageRepository;
use App\Repositories\PlansRepository;
use App\Repositories\ReportsRepository;
use Carbon\Carbon;

class DashboardController extends Controller
{
    public $plan;
    public $report;
    public $coverage;

    /**
     * DashboardController constructor.
     * @param $plan
     * @param $report
     * @param $coverage
     */
    public function __construct(PlansRepository $plan,
                                ReportsRepository $report,
                                CoverageRepository $coverage)
    {
        $this->plan = $plan;
        $this->report = $report;
        $this->coverage = $coverage;
    }


    public function index()
    {
        $startOfMonth = Carbon::now()->startOfMonth()->format('Y-m-d');
        $endOfMonth = Carbon::now()->endOfMonth()->format('Y-m-d');

        $MRId = $this->getMRId();

        /**
         * Get Monthly Coverage
         */
        list($actualCoverage, $allCoverage, $coveragePercent)
            = $this->coverage->getMRCoverageBetween($MRId, $startOfMonth, $endOfMonth);

        /**
         * Get Monthly Reports
         */
        $reports = $this->report->getMRReportsBetween($MRId, $startOfMonth, $endOfMonth);

        return view('mr/dashboard', compact('startOfMonth',
                                            'endOfMonth',
                                            'coveragePercent',
                                            'actualCoverage',
                                            'allCoverage',
                                            'reports'));
    }

    public function getMonthlyHistory()
    {
        return view('mr.search.history');
    }

    public function postMonthlyHistory(HistorySearchRequest $request)
    {
        $monthYear = $request->year.'-'.$request->month;
        $MRId = $this->getMRId();

        $dt = Carbon::createFromFormat('Y-m', $monthYear);
        $startOfMonth = $dt->startOfMonth()->format('Y-m-d');

        $endOfMonth = $dt->endOfMonth()->format('Y-m-d');

        /**
         * Get Monthly Coverage
         */
        list($actualCoverage, $allCoverage, $coveragePercent)
            = $this->coverage->getMRCoverageBetween($MRId, $startOfMonth, $endOfMonth);

        /**
         * Get Monthly Reports
         */
        $reports = $this->report->getMRReportsBetween($MRId, $startOfMonth, $endOfMonth);

        return view('mr/search/history_results', compact('startOfMonth',
            'endOfMonth',
            'coveragePercent',
            'actualCoverage',
            'allCoverage',
            'reports'));
    }

    /**
     * @return mixed
     */
    public function getMRId()
    {
        $MRId = Auth::user()->id;
        return $MRId;
    }
}

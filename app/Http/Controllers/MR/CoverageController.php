<?php

namespace App\Http\Controllers\MR;

use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Http\Requests\MR\CoverageSearchRequest;
use App\Repositories\CoverageRepository;
use App\Repositories\EmployeesRepository;
use App\Repositories\ReportsRepository;

class CoverageController extends Controller
{
    public $coverage;
    public $report;
    public $employee;
    public $customer;

    /**
     * CoverageController constructor.
     * @param CoverageRepository $coverage
     * @param ReportsRepository $report
     * @param EmployeesRepository $employee
     */
    public function __construct(CoverageRepository $coverage,
                                ReportsRepository $report,
                                EmployeesRepository $employee)
    {
        $this->coverage = $coverage;
        $this->report = $report;
        $this->employee = $employee;
    }

    public function coverageSearch()
    {
        return view('mr.search.coverage');
    }

    public function doCoverageSearch(CoverageSearchRequest $request)
    {
        $MRId = Auth::user()->id;

        $coverage = [];

        list($actualCoverage, $allCoverage, $coveragePercent)
            = $this->coverage->getMRCoverageBetween($MRId, $request->from, $request->to);

        $coverage['mr'] = $MR = $this->employee->single($MRId)->name;
        $coverage['actual'] = $actualCoverage;
        $coverage['all'] = $allCoverage;
        $coverage['coverage_percent'] = $coveragePercent;

        return view('mr.search.coverage_results', compact('coverage'));
    }
}

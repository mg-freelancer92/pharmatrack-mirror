<?php
namespace App\Http\Controllers\MR;

use App\Http\Requests\MR\ReportsRequest;
use App\Http\Requests\MR\ReportsSearchRequest;
use App\Reports;
use App\Repositories\EmployeesRepository;
use App\Http\Controllers\Controller;
use App\Repositories\ReportsRepository;
use App\Repositories\ProductsRepository;
use Illuminate\Support\Facades\Auth;

class ReportsController extends Controller
{
    public $report;
    public $employee;
    public $product;
    const VIEW_PATH = 'mr.reports.';

    /**
     * PlansController constructor.
     * @param ReportsRepository $report
     * @param EmployeesRepository $employee
     * @param ProductsRepository $product
     */
    public function __construct(ReportsRepository $report,
                                EmployeesRepository $employee,
                                ProductsRepository $product)
    {
        $this->report = $report;
        $this->employee   = $employee;
        $this->product = $product;
    }

    public function get()
    {
        $mrId = $this->getMrID();
        $reports = $this->report->getMRReports($mrId);
        return view(self::VIEW_PATH.'.all', compact('reports'));
    }

    public function create()
    {
        $mrId = $this->getMrID();
        $customers = $this->employee->getMRCustomers($mrId);
        return view(self::VIEW_PATH.'.add', compact('customers'));
    }

    public function store(ReportsRequest $request)
    {
        try{
            $this->report->create($request);
            return redirect()->back()->with('message','Report has been created successfully !');
        } catch (\Exception $e) {
            return $e;
        }
    }

    public function getMrID()
    {
        return Auth::user()->id;
    }

    public function reportSearch()
    {
        $MRId = Auth::user()->id;
        $customers = $this->employee->getMRCustomers($MRId);
        $products = $this->product->all();
        return view('mr.search.reports', compact('customers', 'products'));
    }

    public function doReportSearch(ReportsSearchRequest $request)
    {
        $MRId = Auth::user()->id;
        $reports = $this->report->search([
            ['created_at', '>=', $request->from],
            ['created_at', '<=', $request->to],
            ['mr_id', '=', $MRId],
            ['customer_id', $request->customer],
            $request->has_follow_up == 1 ? ['follow_up', '<>', NULL] : ['follow_up', NULL],
            'products' => $request->products
        ]);



        return view('mr.search.reports_results', compact('reports'));
    }
}
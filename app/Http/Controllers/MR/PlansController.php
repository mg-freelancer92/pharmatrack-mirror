<?php

namespace App\Http\Controllers\MR;

use App\Http\Requests\MR\PlansRequest;
use App\Repositories\EmployeesRepository;
use App\Repositories\PlansRepository;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\MR\PlansSearchRequest;

class PlansController extends Controller
{
    public $plan;
    public $mr;
    const VIEW_PATH = 'mr.plans.';

    /**
     * PlansController constructor.
     * @param PlansRepository $plan
     * @param EmployeesRepository $mr
     */
    public function __construct(PlansRepository $plan, EmployeesRepository $mr)
    {
        $this->plan = $plan;
        $this->mr = $mr;
    }

    public function get()
    {
        return view(self::VIEW_PATH.'all');
    }

    public function ajaxPlans()
    {
        $plans = array();
        $i = 0;
        $MRId = $this->getMrID();

        foreach ($this->plan->getMRPlans($MRId) as $plan) {
            $plans[$i]['start']     =   $plan->visit_date->format('Y-m-d');
            $plans[$i]['title']     =   ($plan->comment) ? $plan->comment : '';
            $plans[$i]['color']     =   'blue';
            $i++;

            foreach ($plan->customers as $customerInPlan) {
                $plans[$i]['url']   = '';
                $plans[$i]['title'] = $customerInPlan->customer->name;
                $plans[$i]['start'] = $plan->visit_date->format('Y-m-d');
                $plans[$i]['color'] = $this->plan->isDoctorVisited($MRId, $customerInPlan->customer->id, $plan->id) == "true"
                                        ? 'green':'red';
                $i++;
            }
        }
        return json_encode($plans);
    }


    public function create()
    {
        $mrId = $this->getMrID();
        $customers = $this->mr->getMRCustomers($mrId);
        return view(self::VIEW_PATH.'add', compact('customers'));
    }

    public function store(PlansRequest $request)
    {
        try{
            $this->plan->create($request);
            return redirect()->back()->with('message','Plan has been created successfully !');
        } catch (\Exception $e) {
            return $e;
        }
    }

    public function getMrID()
    {
        return Auth::user()->id;
    }

    public function planSearch()
    {
        return view('mr.search.plans');
    }

    public function doPlanSearch(PlansSearchRequest $request)
    {
        $plans = [];
        $MRId = Auth::user()->id;

        $results = $this->plan->search([
            ['visit_date', '>=', $request->from],
            ['visit_date', '<=', $request->to],
            ['mr_id', '=', $MRId]
        ]);

        foreach ($results as $key=>$plan){
            $plans[$key]['id'] = $plan->id;
            $plans[$key]['mr'] = $plan->mr->name;
            $plans[$key]['visit_date'] = $plan->visit_date;
            $plans[$key]['customers'] = $this->plan->getPlanCustomers($plan->id);
        }
        return view('mr.search.plans_results', compact('plans'));
    }
}

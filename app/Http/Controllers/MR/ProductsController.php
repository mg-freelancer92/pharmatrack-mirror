<?php

namespace App\Http\Controllers\MR;

use App\Repositories\ProductsRepository;
use App\Http\Controllers\Controller;

class ProductsController extends Controller
{
    public $product;
    const VIEW_PATH = 'mr.products.';

    /**
     * ProductsController constructor.
     * @param ProductsRepository $product
     */
    public function __construct(ProductsRepository $product)
    {
        $this->product = $product;
    }

    public function get()
    {
        $products = $this->product->all();
        return view(self::VIEW_PATH.'all', compact('products'));
    }
}

<?php

namespace App\Http\Controllers\MR;

use App\Repositories\EmployeesRepository;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class ManagersController extends Controller
{
    public $employee;

    /**
     * ManagersController constructor.
     * @param $employee
     */
    public function __construct(EmployeesRepository $employee)
    {
        $this->employee = $employee;
    }

    public function index()
    {
        $mrId = Auth::user()->id;
        $areaManager = $this->employee->directManager($mrId);
        $salesManagers = $this->employee->directManager($areaManager);

        $areaManagerData = $this->employee->single($areaManager);
        $salesManagerData = $this->employee->single($salesManagers);

        $managers[] = $areaManagerData;
        $managers[] = $salesManagerData;

        return view('mr.managers', compact('managers'));

//        print_r($managers);
    }
}

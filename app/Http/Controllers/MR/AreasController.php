<?php

namespace App\Http\Controllers\MR;

use App\Repositories\AreasRepository;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class AreasController extends Controller
{
    public $area;
    const VIEW_PATH = 'mr.areas.';

    /**
     * AreasController constructor.
     * @param AreasRepository $area
     */
    public function __construct(AreasRepository $area)
    {
        $this->area = $area;
    }

    public function get()
    {
        $MRId = Auth::user()->id;
        $areas = $this->area->getMRAreas($MRId);
        return view(self::VIEW_PATH.'all', compact('areas'));
    }
}

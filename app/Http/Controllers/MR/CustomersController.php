<?php

namespace App\Http\Controllers\MR;
use App\Repositories\EmployeesRepository;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class CustomersController extends Controller
{
    public $employee;

    /**
     * CustomersController constructor.
     * @param $employee
     */
    public function __construct(EmployeesRepository $employee)
    {
        $this->employee = $employee;
    }

    public function index()
    {
        $mrId = Auth::user()->id;
        $customers = $this->employee->getMRCustomers($mrId);
        return view('mr.customers', compact('customers'));
    }
}

<?php
namespace App\Repositories;

use App\Interfaces\ProductsInterface;
use App\Interfaces\TargetsInterface;
use App\Products;
use App\Targets;

/**
 * Class ProductsRepository
 * @package App\Repositories
 */
class ProductsRepository implements ProductsInterface, TargetsInterface{
    /**
     * @return mixed
     */
    public function all()
    {
        return Products::all();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function single($id)
    {
        return Products::findOrFail($id);
    }


    /**
     * @param $data
     * @return \Exception|static
     */
    public function store($data)
    {
        try{
            return Products::create($data);
        }catch (\Exception $e){
            return $e;
        }
    }

    /**
     * @param $id
     * @param $updatedData
     * @return \Exception
     */
    public function update($id, $updatedData)
    {
        try{
            return Products::where('id', $id)->update($updatedData);
        }catch (\Exception $e){
            return $e;
        }
    }

    /**
     * @param $id
     * @return \Exception
     */
    public function destroy($id)
    {
        try{
            return Products::where('id', $id)->delete();
        }catch (\Exception $e){
            return $e;
        }
    }


    /**
     * @param $from
     * @param $to
     * @return mixed
     */
    public function getSalesBetween($from, $to)
    {
        // TODO: Implement getSalesBetween() method.
    }

    /**
     * @return mixed
     */
    public function getProductsBoughtByCustomers()
    {
        // TODO: Implement getProductsBoughtByCustomers() method.
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getProductsBoughtByCustomer($id)
    {
        // TODO: Implement getProductsBoughtByCustomer() method.
    }

    /**
     * @return mixed
     */
    public function getMRsTarget()
    {
        return Targets::all();
    }

    /**
     * @param $MRId
     * @return mixed
     */
    public function getMRTarget($MRId)
    {
        return Targets::where('mr_id', $MRId)->get();
    }

    /**
     * @param $data
     * @return mixed
     */
    public function createMRTarget($data)
    {
        try{
            Targets::create($data);
        }catch (\Exception $e){
            return $e;
        }
    }

    /**
     * @param $MRId
     * @param $productId
     * @param $updatedData
     * @return \Exception
     */
    public function updateMRTarget($MRId, $productId, $updatedData)
    {
        try{
            return Targets::where('mr_id', $MRId)->where('product_id', $productId)->update($updatedData);
        }catch (\Exception $e)
        {
            return $e;
        }
    }

    /**
     * @param $attributes
     * @return mixed
     */
    public function search($attributes)
    {
        return Products::where($attributes)->get();
    }
}
<?php
namespace App\Repositories;

use App\Interfaces\SalesInterface;
use App\ReportProducts;
use App\Targets;

class SalesRepository implements SalesInterface{

    public function getMRTarget($MRId, $productId = null)
    {
        $month = lcfirst(date('M'));
        $query = Targets::where(['year' => date('Y'), 'mr_id' => $MRId]);

        if(!is_null($productId)) $query = $query->where('product_id', $productId)->first()->$month;

        return $query->get();
    }

    public function getMRSales($MRId, $productId = null, $start, $end)
    {
        $query = ReportProducts::join('reports', 'reports.id', '=', 'report_products.report_id')
            ->where('product_action', 'sell')
            ->where('mr_id', $MRId)
            ->where('reports.created_at','>=', $start)
            ->where('reports.created_at','<=', $end);
        
        if (! is_null($productId)) return $query->where('product_id', $productId)
                                                    ->sum('sold_products_count');
        
        return $query->get();
    }

    public function getMRSalesForCustomer($MRId, $customerId)
    {
        // TODO: Implement getMRSalesForCustomer() method.
    }

    public function getMRSalesBetween($MRId, $from, $to)
    {
        // TODO: Implement getMRSalesBetween() method.
    }
}
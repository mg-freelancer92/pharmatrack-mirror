<?php
namespace App\Repositories;

use App\Areas;
use App\Employees;
use App\Interfaces\AreasInterface;

class AreasRepository implements AreasInterface{

    public $employee;

    /**
     * AreasRepository constructor.
     * @param $employee
     */
    public function __construct(EmployeesRepository $employee)
    {
        $this->employee = $employee;
    }

    /**
     * @return mixed
     */
    public function all()
    {
        return Areas::all();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function single($id)
    {
        return Areas::findOrFail($id);
    }


    /**
     * @param $data
     * @return mixed
     */
    public function store($data)
    {
        try{
            return Areas::create($data);    
        }catch (\Exception $e)
        {
            return $e;
        }
    }

    /**
     * @param $id
     * @param $updatedData
     * @return mixed
     */
    public function update($id, $updatedData)
    {
        try{
            return Areas::where('id', $id)->update($updatedData);
        }catch (\Exception $e)
        {
            return $e;
        }
    }

    /**
     * @param $id
     * @return mixed
     */
    public function destroy($id)
    {
        try{
            return Areas::where('id', $id)->delete();
        }catch (\Exception $e)
        {
            return $e;
        }
    }

    public function getMRAreas($id)
    {
        $AMId = $this->employee->directManager($id);
        return Areas::where('am_id', $AMId)->get();
    }
}
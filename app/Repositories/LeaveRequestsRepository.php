<?php
namespace App\Repositories;

use App\Interfaces\LeaveRequestsInterface;
use App\LeaveRequests;

class LeaveRequestsRepository implements LeaveRequestsInterface{
    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function all()
    {
        return LeaveRequests::all();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function approve($id)
    {
        return LeaveRequests::where('id', $id)->update(['approved' => 1]);
    }

    /**
     * @param $id
     * @param $reason
     * @return mixed
     */
    public function decline($id, $reason)
    {
        return LeaveRequests::where('id', $id)->update(['approved' => 0, 'reason' => $reason]);
    }


    /**
     * @return mixed
     */
    public function approvedLeaveRequests()
    {
        return LeaveRequests::approved()->get();
    }

    /**
     * @return mixed
     */
    public function declinedLeaveRequests()
    {
        return LeaveRequests::declined()->get();
    }

    /**
     * @param $from
     * @param $to
     * @return mixed
     */
    public function getLeaveRequestsBetween($from, $to)
    {
        return LeaveRequests::between($from, $to);
    }

    /**
     * @param $MRId
     * @return mixed
     */
    public function getMRLeaveRequests($MRId)
    {
        return LeaveRequests::where('mr_id', $MRId)->approved()->get();
    }

    /**
     * @param $MRsId
     * @return mixed
     */
    public function getMRsLeaveRequests($MRsId)
    {
        return LeaveRequests::whereIn('mr_id', $MRsId)->get();
    }

    /**
     * @return mixed
     */
    public function getApprovedLeaveRequests()
    {
        return LeaveRequests::approved()->get();
    }

    /**
     * @return mixed
     */
    public function getDeclinedLeaveRequests()
    {
        return LeaveRequests::declined()->get();
    }

    /**
     * @param $attributes
     * @return mixed
     */
    public function search($attributes)
    {
        return LeaveRequests::where($attributes)->get();
    }

    public function create($attributes)
    {
        return LeaveRequests::create($attributes->toArray());
    }
}
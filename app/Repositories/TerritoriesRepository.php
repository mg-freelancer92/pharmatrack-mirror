<?php
namespace App\Repositories;

use App\Interfaces\TerritoriesInterface;
use App\Territories;

class TerritoriesRepository implements TerritoriesInterface{
    /**
     * @return mixed
     */
    public function all()
    {
        return Territories::all();
    }

    /**
     * @param $data
     * @return mixed
     */
    public function store($data)
    {
        try{
            return Territories::create($data);
        }catch (\Exception $e)
        {
            return $e;
        }
    }

    /**
     * @param $id
     * @param $updatedData
     * @return mixed
     */
    public function update($id, $updatedData)
    {
        try{
            return Territories::where('id', $id)->update($updatedData);
        }catch (\Exception $e)
        {
            return $e;
        }
    }

    /**
     * @param $id
     * @return mixed
     */
    public function destroy($id)
    {
        try{
            return Territories::where('id', $id)->delete();
        }catch (\Exception $e)
        {
            return $e;
        }
    }
}
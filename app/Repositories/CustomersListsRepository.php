<?php
namespace App\Repositories;

use App\Customers;
use App\Interfaces\CustomersListsInterface;

/**
 * Class CustomersListsRepository
 * @package App\Repositories
 */
class CustomersListsRepository implements CustomersListsInterface{

    /**
     * @param $MRId
     * @param $data
     * @return \Exception
     */
    public function upload($MRId, $data)
    {
        try {
            \Excel::load(public_path('uploads/doctors_list/' . $MRId . '/doctors_list.xlsx'), function ($reader)
            use ($MRId, $data) {
                $results = $reader->get();
                $reader->ignoreEmpty();
                foreach ($results as $row) {
                    if (isset($row)) {
                        try{
                            Customers::create($data);
                        }catch (\Exception $e){
                            return $e;
                        }
                    }
                }
            });
        }catch (\Exception $e) {
            return $e;
        }
    }
}
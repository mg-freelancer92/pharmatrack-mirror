<?php
namespace App\Repositories;

use App\Customers;
use App\Interfaces\CoverageInterface;

class CoverageRepository implements CoverageInterface{

    public $report;

    /**
     * CoverageRepository constructor.
     * @param $report
     */
    public function __construct(ReportsRepository $report)
    {
        $this->report = $report;
    }


    /**
     * @param $MRId
     * @return mixed
     */
    public function getMRCoverage($MRId)
    {
        return Customers::join('visits_classes', 'customers.class_id', '=', 'visits_classes.id')
                        ->where('mr_id', $MRId)
                        ->sum('visits_count');
    }

    /**
     * @param $MRId
     * @param $from
     * @param $to
     * @return mixed
     */
    public function getMRCoverageBetween($MRId, $from, $to)
    {
        $actualCoverage = count($this->report->getMRReportsBetween($MRId, $from, $to));
        $allCoverage = $this->getMRCoverage($MRId);

        $coveragePercent = $allCoverage != 0
            ? floatval(number_format(($actualCoverage / $allCoverage) * 100, 2)) : 0;
        return [$actualCoverage, $allCoverage, $coveragePercent];
    }

    /**
     * @param $MRId
     * @param $customerId
     * @return mixed
     */
    public function getMRCoverageForCustomer($MRId, $customerId)
    {
        return Customers::join('visits_classes', 'customers.class_id', '=', 'visits_classes.id')
            ->where('customers.id', $customerId)
            ->where('mr_id', $MRId)
            ->sum('visits_count');
    }
}
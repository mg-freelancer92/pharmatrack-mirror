<?php
namespace App\Repositories;

use App\Customers;
use App\Interfaces\CustomersInterface;

/**
 * Class CustomersRepository
 * @package App\Repositories
 */
class CustomersRepository implements CustomersInterface{

    /**
     * @return mixed
     */
    public function all()
    {
        return Customers::all();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function single($id)
    {
        return Customers::findOrFail($id);
    }


    /**
     * @param $data
     * @return mixed
     */
    public function store($data)
    {
        try{
            return Customers::create($data);
        } catch (\Exception $e){
            return $e;
        }
    }

    /**
     * @param $id
     * @param $updatedData
     * @return mixed
     */
    public function update($id, $updatedData)
    {
        try{
            return Customers::where('id', $id)->update($updatedData);
        }catch (\Exception $e) {
            return $e;
        }
    }

    /**
     * @param $id
     * @return mixed
     */
    public function destroy($id)
    {
        try{
            Customers::findOrFail($id)->delete();
        }catch (\Exception $e){
            return $e;
        }
    }

    /**
     * @param $attributes
     * @return mixed
     */
    public function search($attributes)
    {
        return Customers::where($attributes)->get();
    }
}
<?php
namespace App\Repositories;

use App\Interfaces\PlansInterface;
use App\Plans;
use App\PlansCustomers;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use App\Reports;

class PlansRepository implements PlansInterface{
    /**
     * @return mixed
     */
    public function all()
    {
        Plans::all();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function single($id)
    {
        return Plans::findOrFail($id);
    }

    /**
     * @param $from
     * @param $to
     * @return mixed
     */
    public function getPlansBetween($from, $to)
    {
        Plans::between($from, $to)->get();
    }

    /**
     * @param $MRId
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getMRPlans($MRId)
    {
        return Plans::where('mr_id', $MRId)->get();
    }

    /**
     * @return mixed
     */
    public function getApprovedPlans()
    {
        return Plans::approved()->get();
    }

    /**
     * @return mixed
     */
    public function getDeclinedPlans()
    {
        return Plans::declined()->get();
    }

    /**
     * @param $attributes
     * @return mixed
     */
    public function search($attributes)
    {
        return Plans::where($attributes)->get();
    }

    public function getPlanCustomers($planId)
    {
        return PlansCustomers::select('customer_id')->where('plan_id', $planId)->get();
    }


    /**
     * @param $attributes
     */
    public function create($attributes)
    {
        $customersToVisit = $attributes['customers'];
        unset($attributes['customers']);
        DB::transaction(function() use ($attributes, $customersToVisit){
            $plan = Plans::create($attributes->toArray());
            foreach($customersToVisit as $customer)
            {
                PlansCustomers::create([
                    'plan_id' => $plan->id,
                    'customer_id' => $customer
                ]);
            }
        });
    }

    public function isDoctorVisited($mrId, $customerId, $planId)
    {
        $planDays[] = Plans::join('plans_customers', 'plans.id', '=', 'plans_customers.plan_id')
            ->select('visit_date')
            ->where('mr_id', $mrId)
            ->where('customer_id', $customerId)
            ->where('plans_customers.plan_id', $planId)
            ->orderBy('visit_date', 'DESC')
            ->get()
            ->toArray();


        $reportDays[] = Reports::select('created_at')
            ->where('mr_id', $mrId)
            ->where('customer_id', $customerId)
            ->orderBy('created_at', 'DESC')
            ->get()
            ->toArray();


        foreach($planDays as $key=>$planDay){
            $dt = Carbon::createFromFormat('Y-m-d H:i:s', $planDay[$key]['visit_date']);
            $startOfWeek = $dt->startOfWeek();

            $dt = Carbon::createFromFormat('Y-m-d H:i:s', $planDay[$key]['visit_date']);
            $endOfWeek = $dt->endOfWeek();

            foreach($reportDays as $reportDay) {
                if (isset($reportDay[$key]) && $reportDay[$key]['created_at'] >= $startOfWeek
                    && $reportDay[$key]['created_at'] <= $endOfWeek) {
                    return "true";
                }
            }
        }
    }


}
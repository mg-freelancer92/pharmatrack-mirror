<?php
namespace App\Repositories;

use App\Interfaces\ReportsInterface;
use App\ReportProducts;
use App\Reports;
use Illuminate\Support\Facades\DB;

class ReportsRepository implements ReportsInterface{

    /**
     * @return mixed
     */
    public function all()
    {
        return Reports::all();
    }

    /**
     * @param $from
     * @param $to
     * @return mixed
     */
    public function getReportsBetween($from, $to)
    {
        return Reports::between($from, $to)->get();
    }

    /**
     * @param $MRId
     * @param $start
     * @param $end
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getMRReports($MRId, $start=null, $end=null)
    {
        return Reports::with('products')->where('mr_id', $MRId)->orderBy('created_at', 'DESC')->get();
    }

    /**
     * @param $MRId
     * @param $start
     * @param $end
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getMRReportsBetween($MRId, $start, $end)
    {
        return Reports::with('products')->where('created_at', '>=', $start)
                                        ->where('created_at', '<=', $end)
                                        ->where('mr_id', $MRId)
                                        ->get();
    }

    /**
     * @param $MRId
     * @param $customerID
     * @param $start
     * @param $end
     * @return mixed
     */
    public function getMRReportsForCustomer($MRId, $customerID, $start, $end)
    {
        return Reports::where('mr_id',$MRId)
                        ->where('customer_id', $customerID)
                        ->where('created_at','>=', $start)
                        ->where('created_at','<=', $end)
                        ->count();
    }

    /**
     * @return mixed
     */
    public function getMRsVisitCoverage()
    {
        // TODO: Implement getMRsVisitCoverage() method.
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getMRVisitCoverage($id)
    {
        // TODO: Implement getMRVisitCoverage() method.
    }

    /**
     * @param $attributes
     * @return mixed
     */
    public function search($attributes)
    {
        $reports = [];
        $products = $attributes['products'];
        unset($attributes['products']);

        $results = Reports::where($attributes)->get();

        foreach($results->toArray() as $report){
            $reports = Reports::join('report_products', 'reports.id', '=', 'report_products.report_id')
                ->select('reports.id as id', 'customer_id', 'reports.created_at as created_at', 'follow_up')
                ->whereIn('product_id', $products)
                ->where('report_products.id', $report['id'])
                ->get();
        }

        return $reports;
    }

    public function getReportProducts($reportId)
    {
        return ReportProducts::select('product_id')->where('report_id', $_REQUEST)->get();
    }


    /**
     * @param $attributes
     * @return mixed
     */
    public function create($attributes)
    {
        $samplesProducts = $attributes['samples_products'];
        unset($attributes['samples_products']);
        DB::transaction(function() use ($attributes, $samplesProducts){
            $report = Reports::create($attributes->toArray());
            foreach($samplesProducts as $product)
            {
                ReportProducts::create([
                    'report_id'     => $report->id,
                    'product_id'    => $product,
                    'product_action' =>  'sample'
                ]);
            }
        });
    }


}
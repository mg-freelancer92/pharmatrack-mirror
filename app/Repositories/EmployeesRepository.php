<?php
namespace App\Repositories;

use App\Customers;
use App\Employees;
use App\Interfaces\EmployeesInterface;

/**
 * Class EmployeesRepository
 * @package App\Repositories
 */
class EmployeesRepository implements EmployeesInterface{

    /**
     * @param $position
     * @param null $attributes
     * @return mixed
     */
    public function all($position, $attributes = null)
    {
        return Employees::$position()->get($attributes);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function single($id)
    {
        return Employees::findOrFail($id);
    }


    /**
     * @param $data
     * @return \Exception
     */
    public function store($data)
    {
        try{
            Employees::create($data);
        } catch (\Exception $e){
            return $e;
        }
    }

    /**
     * @param $id
     * @param $updatedData
     * @return \Exception
     */
    public function update($id, $updatedData)
    {
        try{
            return Employees::where('id', $id)->update($updatedData);
        }catch (\Exception $e) {
            return $e;
        }
    }

    /**
     * @param $id
     * @return \Exception
     */
    public function destroy($id)
    {
        try{
            Employees::findOrFail($id)->delete();
        }catch (\Exception $e){
            return $e;
        }
    }
    

    /**
     * @param $id
     * @return mixed
     */
    public function getSMCustomers($id)
    {
        $AMs = Employees::select('id')->where('manager_id', $id)->get();
        $MRs = Employees::select('id')->whereIn('manager_id', $AMs)->get();
        return Customers::whereIn('mr_id', $MRs)->get();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getAMCustomers($id)
    {
        $MRs = Employees::select('id')->whereIn('manager_id', $id)->get();
        return Customers::whereIn('mr_id', $MRs)->get();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getMRCustomers($id)
    {
        return Customers::where('mr_id', $id)->get();
    }

    /**
     * @param $attributes
     * @return mixed
     */
    public function search($attributes)
    {
        return Employees::where($attributes)->get();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function directManager($id)
    {
        return Employees::findOrFail($id)->manager_id;
    }

    public function getSMMRs($SMId, $attributes = null)
    {
        $AMsIds = $this->getSMAMs($SMId, ['id']);
        return Employees::select(!is_null($attributes) ? $attributes : '*')->
                          whereIn('manager_id', $AMsIds)->get();
    }

    public function getSMAMs($SMId, $attributes = null)
    {
        return Employees::select(!is_null($attributes) ? $attributes : '*')->
                         where('manager_id', $SMId)->get();
    }

    public function getAMMRs($AMId, $attributes = null)
    {
        return Employees::select(!is_null($attributes) ? $attributes : '*')->
                         where('manager_id', $AMId)->get();
    }
}
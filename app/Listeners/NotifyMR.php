<?php

namespace App\Listeners;

use App\Events\LeaveRequestWasReviewed;
use Illuminate\Support\Facades\View;

class NotifyMR
{
    /**
     * Create the event listener.
     */
    public function __construct()
    {
        
    }

    public function handle(LeaveRequestWasReviewed $event)
    {
        $statue = 'Declined';
        if ($event->leaveRequest->approved)
        {
            $statue = 'Approved';
        }

        \Mail::send('mr.emails.leave_requests', ['event' => $event], function ($m) use ($event, $statue) {
            $m->from('me@mg-freelancer.com');
            $m->to($event->leaveRequest->mr->email)
                ->subject('Your Leave Request on '.$event->leaveRequest->created_at->format('d-m-Y') .
                ' has been '. $statue);
        });
    }
}
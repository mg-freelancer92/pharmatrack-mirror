<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class Reports extends Model
{
    use SoftDeletes;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'mr_id', 
        'customer_id', 
        'total_sold_products_price', 
        'feedback', 
        'follow_up', 
        'double_visit_manager_id', 
        'is_planned', 
        'lat', 
        'lon' 
    ];
    
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];
    
    public function products()
    {
        return $this->hasMany(ReportProducts::class, 'report_id');
    }

    public function customer()
    {
        return $this->belongsTo(Customers::class, 'customer_id');
    }
    
    public function doubleVisitManagerId()
    {
        return $this->belongsTo(Employees::class, 'double_visit_manager_id');
    }

    /**
     * @param $value
     * @return string
     */
    public function getIsPlannedAttribute($value)
    {
        if ($value) return "<span class='label label-success'>Planned</span>";
        return "<span class='label label-danger'>Not Planned</span>";
    }
    
    public function scopeBetween($query, $from, $to)
    {
        return $query->whereBetween('created_at', [$from, $to])->get();
    }

    public function getCreatedAtAttribute($date)
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('Y-m-d');
    }

    public function getUpdatedAtAttribute($date)
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('Y-m-d');
    }
}

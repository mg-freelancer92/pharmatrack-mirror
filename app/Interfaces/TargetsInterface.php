<?php
namespace App\Interfaces;

interface TargetsInterface{

    public function getMRsTarget();

    public function getMRTarget($MRId);

    public function createMRTarget($data);

    public function updateMRTarget($MRId, $productId, $updatedData);

    public function search($attributes);
}
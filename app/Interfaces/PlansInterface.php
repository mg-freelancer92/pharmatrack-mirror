<?php
namespace App\Interfaces;

interface PlansInterface{
    public function all();
    
    public function single($id);

    public function getPlansBetween($from, $to);
    
    public function getMRPlans($MRId);

    public function getApprovedPlans();

    public function getDeclinedPlans();

    public function search($attributes);

    public function getPlanCustomers($planId);

    public function create($attributes);

    public function isDoctorVisited($mrId, $customerId, $planId);
}
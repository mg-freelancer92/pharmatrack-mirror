<?php
namespace App\Interfaces;

interface CustomersInterface{
    public function all();
    
    public function single($id);
    
    public function store($data);
    
    public function update($id, $updatedData);
    
    public function destroy($id);

    public function search($attributes);
}
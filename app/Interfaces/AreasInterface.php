<?php
namespace App\Interfaces;

interface AreasInterface{
    public function all();

    public function single($id);

    public function store($data);
    
    public function update($id, $updatedData);
    
    public function destroy($id);

    public function getMRAreas($id);
}
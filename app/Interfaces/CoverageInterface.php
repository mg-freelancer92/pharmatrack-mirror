<?php
namespace App\Interfaces;

interface CoverageInterface{
    public function getMRCoverage($MRId);

    public function getMRCoverageForCustomer($MRId, $customerId);
    
    public function getMRCoverageBetween($MRId, $from, $to);

}
<?php
namespace App\Interfaces;

interface ReportsInterface{
    public function all();

    public function getReportsBetween($from, $to);
    
    public function getMRReports($MRId);
    
    public function getMRReportsBetween($MRId, $start, $end);
    
    public function getMRReportsForCustomer($MRId, $customerID, $start, $end);
    
    public function getMRsVisitCoverage();

    public function getMRVisitCoverage($MRId);

    public function search($attributes);

    public function getReportProducts($reportId);

    public function create($attributes);
}
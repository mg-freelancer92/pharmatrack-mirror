<?php
namespace App\Interfaces;

interface ProductsInterface {
    public function all();

    public function single($id);
    
    public function store($id);

    public function update($id, $updatedData);
    
    public function destroy($id);
    
    public function getSalesBetween($from, $to);

    public function getProductsBoughtByCustomers();

    public function getProductsBoughtByCustomer($id);

    public function search($attributes);
}
<?php
namespace App\Interfaces;

interface LeaveRequestsInterface{
    public function all();

    public function approve($id);

    public function decline($id, $reason);

    public function approvedLeaveRequests();

    public function declinedLeaveRequests();

    public function getLeaveRequestsBetween($from, $to);
    
    public function getMRLeaveRequests($MRId);

    public function getMRsLeaveRequests($MRsId);

    public function getApprovedLeaveRequests();

    public function getDeclinedLeaveRequests();

    public function search($attributes);

    public function create($attributes);
}
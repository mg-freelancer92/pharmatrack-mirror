<?php
namespace App\Interfaces;

interface SalesInterface{
    public function getMRTarget($MRId, $productId = null);
    
    public function getMRSales($MRId, $productId = null, $start, $end);

    public function getMRSalesForCustomer($MRId, $customerId);
    
    public function getMRSalesBetween($MRId, $from, $to);

}
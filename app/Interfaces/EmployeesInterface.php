<?php
namespace App\Interfaces;

interface EmployeesInterface{
    public function all($position, $attributes = null);
    
    public function single($id);
    
    public function store($data);
    
    public function update($id, $updatedData);
    
    public function destroy($id);
    
    public function getSMCustomers($id);
    
    public function getAMCustomers($id);
    
    public function getMRCustomers($id);
    
    public function search($attributes);

    public function directManager($id);

    public function getSMMRs($SMId, $attributes = null);

    public function getSMAMs($SMId, $attributes = null);

    public function getAMMRs($AMId, $attributes = null);
}
<?php
namespace App\Interfaces;

interface TerritoriesInterface{
    public function all();
    
    public function store($data);
    
    public function update($id, $updatedData);
    
    public function destroy($id);
}
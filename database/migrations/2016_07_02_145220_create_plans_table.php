<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plans', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('mr_id')->unsigned();
            $table->date('visit_date');
            $table->text('comment');
            $table->text('decline_reason');
            $table->boolean('approved')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::table('plans', function ($table) {
            // Constraints
            $table->foreign('mr_id')->references('id')->on('employees');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('plans');
    }
}

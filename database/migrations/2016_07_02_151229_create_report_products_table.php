<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReportProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('report_products', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('report_id')->unsigned();
            $table->integer('product_id')->unsigned();
            $table->enum('product_action', ['sell', 'promote', 'sample']);
            $table->integer('sold_products_count')->unsigned()->nullable();
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::table('report_products', function ($table) {
            // Constraints
            $table->foreign('report_id')->references('id')->on('reports');
            $table->foreign('product_id')->references('id')->on('products');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('report_products');
    }
}

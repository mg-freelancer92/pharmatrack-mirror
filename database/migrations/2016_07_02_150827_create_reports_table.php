<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reports', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('mr_id')->unsigned();
            $table->integer('customer_id')->unsigned();
            $table->float('total_sold_products_price', 11, 3)->nullable();
            $table->text('feedback')->nullable();
            $table->text('follow_up')->nullable();
            $table->integer('double_visit_manager_id')->nullable()->unsigned()->nullable();
            $table->boolean('is_planned')->default(1);
            $table->string('lat')->nullable();
            $table->string('lon')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::table('reports', function ($table) {
            // Constraints
            $table->foreign('mr_id')->references('id')->on('employees');
            $table->foreign('double_visit_manager_id')->references('id')->on('employees');
            $table->foreign('customer_id')->references('id')->on('customers');
        });
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('reports');
    }
}
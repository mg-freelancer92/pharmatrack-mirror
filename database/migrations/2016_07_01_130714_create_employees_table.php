<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('manager_id')->nullable()->unsigned();
            $table->string('name');
            $table->string('email');
            $table->string('mobile');
            $table->integer('level_id')->nullable()->unsigned();
            $table->date('hiring_date')->nullable();
            $table->date('leaving_date')->nullable();;
            $table->boolean('status');
            $table->string('password');
            $table->rememberToken();
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::table('employees', function ($table) {
            // Constraints
            $table->foreign('manager_id')->references('id')->on('employees');
            $table->foreign('level_id')->references('id')->on('levels');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('employees');
    }
}

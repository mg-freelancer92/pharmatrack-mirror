<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('class_id')->unsigned();
            $table->integer('mr_id')->unsigned();
            $table->string('email')->nullable();
            $table->string('mobile')->nullable();
            $table->string('clinic_phone')->nullable();
            $table->enum('working_place', ['clinic', 'polyclinic', 'hospital', 'pharmacy', 'medical_center'])->nullable();
            $table->string('working_place_name')->nullable();
            $table->enum('specialty', ['GYN', 'IM', 'GP', 'Surg', 'Orth', 'Ped', 'Ophth', 'VS']);
            $table->text('address')->nullable();
            $table->text('address_description')->nullable();
            $table->text('working_time')->nullable();
            $table->text('time_of_visit')->nullable();
            $table->text('comment')->nullable();
            $table->boolean('status');
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::table('customers', function ($table) {
            // Constraints
            $table->foreign('mr_id')->references('id')->on('employees');
            $table->foreign('class_id')->references('id')->on('visits_classes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('customers');
    }
}

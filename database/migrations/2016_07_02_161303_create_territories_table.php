<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTerritoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('territories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('area_id')->unsigned();
            $table->integer('mr_id')->unsigned()->nullable();
            $table->text('description');
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::table('territories', function ($table) {
            // Constraints
            $table->foreign('area_id')->references('id')->on('areas');
            $table->foreign('mr_id')->references('id')->on('employees');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('territories');
    }
}

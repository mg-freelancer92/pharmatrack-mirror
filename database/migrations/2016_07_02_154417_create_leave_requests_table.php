<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLeaveRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('leave_requests', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('mr_id')->unsigned();
            $table->text('reason');
            $table->date('leave_start');
            $table->date('leave_end');
            $table->boolean('approved')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::table('leave_requests', function ($table) {
            // Constraints
            $table->foreign('mr_id')->references('id')->on('employees');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('leave_requests');
    }
}

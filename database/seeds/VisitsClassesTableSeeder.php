<?php

use Illuminate\Database\Seeder;

class VisitsClassesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('visits_classes')->insert([
            'name'              =>  'A+',
            'visits_count'      =>  '5',
            'created_at'        => \Carbon\Carbon::now()->toDateTimeString(),
            'updated_at'        => \Carbon\Carbon::now()->toDateTimeString()
        ]);
        
        DB::table('visits_classes')->insert([
            'name'              =>  'A',
            'visits_count'      =>  '4',
            'created_at'        => \Carbon\Carbon::now()->toDateTimeString(),
            'updated_at'        => \Carbon\Carbon::now()->toDateTimeString()
        ]);

        DB::table('visits_classes')->insert([
            'name'              =>  'B',
            'visits_count'      =>  '3',
            'created_at'        => \Carbon\Carbon::now()->toDateTimeString(),
            'updated_at'        => \Carbon\Carbon::now()->toDateTimeString()
        ]);

        DB::table('visits_classes')->insert([
            'name'              =>  'C',
            'visits_count'      =>  '2',
            'created_at'        => \Carbon\Carbon::now()->toDateTimeString(),
            'updated_at'        => \Carbon\Carbon::now()->toDateTimeString()
        ]);

        DB::table('visits_classes')->insert([
            'name'              =>  'D',
            'visits_count'      =>  '1',
            'created_at'        => \Carbon\Carbon::now()->toDateTimeString(),
            'updated_at'        => \Carbon\Carbon::now()->toDateTimeString()
        ]);
    }
}
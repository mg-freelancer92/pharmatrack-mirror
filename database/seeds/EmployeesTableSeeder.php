<?php

use Illuminate\Database\Seeder;

class EmployeesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('employees')->insert([
            'name'          =>  'Sales Manager',
            'manager_id'    =>  NULL,
            'email'         =>  'sales@gmail.com',
            'password'      =>  Hash::make('password'),
            'level_id'      =>  1,
            'hiring_date'   =>  \Carbon\Carbon::createFromDate(2016, 07, 1),
            'leaving_date'  =>  '',
            'status'        =>  1,
            'created_at'    => \Carbon\Carbon::now()->toDateTimeString(),
            'updated_at'    => \Carbon\Carbon::now()->toDateTimeString()
        ]);

        DB::table('employees')->insert([
            'name'          =>  'Area Manager',
            'manager_id'    =>  1,
            'email'         =>  'area@gmail.com',
            'password'      =>  Hash::make('password'),
            'level_id'      =>  2,
            'hiring_date'   =>  \Carbon\Carbon::createFromDate(2016, 07, 2),
            'leaving_date'  =>  '',
            'status'        =>  1,
            'created_at'    => \Carbon\Carbon::now()->toDateTimeString(),
            'updated_at'    => \Carbon\Carbon::now()->toDateTimeString()
        ]);

        DB::table('employees')->insert([
            'name'          =>  'Medical Rep',
            'manager_id'    =>  2,
            'email'         =>  'medical@gmail.com',
            'password'      =>  Hash::make('password'),
            'level_id'      =>  3,
            'hiring_date'   =>  \Carbon\Carbon::createFromDate(2016, 07, 2),
            'leaving_date'  =>  '',
            'status'        =>  1,
            'created_at'    => \Carbon\Carbon::now()->toDateTimeString(),
            'updated_at'    => \Carbon\Carbon::now()->toDateTimeString()
        ]);
    }
}

<?php

use Illuminate\Database\Seeder;

class CustomersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('customers')->insert([
            'name'                  =>  'Ali Mohamed',
            'mr_id'                 =>  3,
            'class'                 =>  'A',
            'email'                 =>  'ali-mohamed@gmail.com',
            'working_place'         =>  'clinic',
            'working_place_name'    =>  'Clinic Name',
            'specialty'             =>  'GYN',
            'mobile'                =>  '01014235842',
            'clinic_phone'          =>  '023822901',
            'address'               =>  'Gamal AbdelNasser, Faysel, Giza',
            'address_description'   =>  'Front of Ambulance Station',
            'working_time'          =>  'From 9:00 AM To 5:00 PM except Friday',
            'time_of_visit'         =>  'From 3:30 PM To 5:00 PM except Sunday',
            'active'                =>  1,
            'created_at'            => \Carbon\Carbon::now()->toDateTimeString(),
            'updated_at'            => \Carbon\Carbon::now()->toDateTimeString()
        ]);
    }
}

@extends('mr.layouts.main')

@section('title') Your Customers @endsection
@section('content')
<!-- Page content -->
<div id="page-content">
    <!-- Datatables Header -->
    <ul class="breadcrumb breadcrumb-top">
        <li>Tables</li>
        <li><a href="">Datatables</a></li>
    </ul>
    <!-- END Datatables Header -->

    <!-- Datatables Content -->
    <div class="block full">
        <div class="block-title">
            <h2><strong>Your </strong> Customers </h2>
        </div>

        <div class="table-responsive">
            <table class="table table-vcenter table-condensed table-bordered datatable">
                <thead>
                <tr>
                    <th class="text-center">Name</th>
                    <th class="text-center">Class</th>
                    <th>Mobile</th>
                    <th>Clinic Phone</th>
                    <th>Working Place</th>
                    <th>Working Place Name</th>
                    <th>Address</th>
                    <th>Status</th>
                    <th class="text-center">Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach($customers as $customer)
                    <tr>
                        <td class="text-center">{{$customer->name}}</td>
                        <td class="text-center">{{$customer->visitClass->name}}</td>
                        <td class="text-center">{{$customer->mobile}}</td>
                        <td class="text-center">{{$customer->clinic_phone}}</td>
                        <td class="text-center">{{$customer->working_place}}</td>
                        <td class="text-center">{{$customer->working_place_name}}</td>
                        <td class="text-center">{{$customer->address}}</td>
                        <td class="text-center">{!! $customer->status !!}</td>
                        <td class="text-center">
                            <div class="btn-group">
                                <a href="{{url("sm/customers/$customer->id/edit")}}" data-toggle="tooltip" title="Edit" class="btn btn-xs btn-default"><i class="fa fa-pencil"></i></a>
                                <a data-toggle="modal" data-target="#mr_{{$customer->id}}"title="Delete" class="btn btn-xs btn-danger"><i class="fa fa-times"></i></a>
                            </div>
                        </td>
                    </tr>
                @endforeach

                </tbody>
            </table>
        </div>
    </div>
    <!-- END Datatables Content -->
</div>
<!-- END Page Content -->
@endsection

@section('custom_scripts')
<!-- Load and execute javascript code used only in this page -->
<script src="{{URL::asset('js/pages/tablesDatatables.js')}}"></script>
<script>
    $(function () {
        TablesDatatables.init();
    });
    $('#mrs_tab').addClass('active');
</script>
@endsection
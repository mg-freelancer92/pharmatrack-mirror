<div id="sidebar">
    <!-- Wrapper for scrolling functionality -->
    <div id="sidebar-scroll">
        <!-- Sidebar Content -->
        <div class="sidebar-content">                           
            <!-- User Info -->
            <div class="sidebar-section sidebar-user clearfix sidebar-nav-mini-hide">
                <div class="sidebar-user-avatar">
                    <a href="page_ready_user_profile.html">
                        <img src="{{URL::asset('img/placeholders/avatars/avatar2.jpg')}}" alt="avatar">
                    </a>
                </div>
                <div class="sidebar-user-name">{{Auth::user()->name}}</div>
                <div class="sidebar-user-links">
                    <a href="page_ready_user_profile.html" data-toggle="tooltip" data-placement="bottom" title="Profile"><i class="gi gi-user"></i></a>
                    <a href="page_ready_inbox.html" data-toggle="tooltip" data-placement="bottom" title="Messages"><i class="gi gi-envelope"></i></a>
                    <a href="login.html" data-toggle="tooltip" data-placement="bottom" title="Logout"><i class="gi gi-exit"></i></a>
                </div>
            </div>
            <!-- END User Info -->

            <!-- Sidebar Navigation -->
            <ul class="sidebar-nav">
                <li id="dashboard_tab">
                    <a href="{{url('mr/dashboard')}}"><i class="fa fa-tachometer sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Dashboard</span></a>
                </li>

                <li id="history_tab">
                    <a href="{{url('mr/history/search')}}"><i class="fa fa-clock-o sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Monthly History</span></a>
                </li>

                <li class="sidebar-header">
                    <span class="sidebar-header-options clearfix"><i class="fa fa-users"></i></span>
                    <span class="sidebar-header-title">Users</span>
                </li>
                <li id="managers_tab">
                    <a href="{{url('mr/managers')}}"><i class="fa fa-users sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Managers</span></a>
                </li>
                <li id="customers_tab">
                    <a href="{{url('mr/customers')}}"><i class="fa fa-fa fa-stethoscope sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Customers</span></a>
                </li>
                <li id="areas_tab">
                    <a href="{{url('mr/areas')}}">
                        <i class="fa fa-fa fa-road sidebar-nav-icon"></i>
                        <span class="sidebar-nav-mini-hide">Areas</span>
                    </a>
                </li>
                <li id="products">
                    <a href="{{url('mr/products')}}">
                        <i class="fa fa-fa fa-cube sidebar-nav-icon"></i>
                        <span class="sidebar-nav-mini-hide">Products</span>
                    </a>
                </li>
                <li class="sidebar-header">
                    <span class="sidebar-header-options clearfix"><i class="fa fa-refresh"></i></span>
                    <span class="sidebar-header-title">Updates</span>
                </li>

                <li id="reports_tab">
                    <a href="{{url('mr/reports')}}">
                        <i class="fa fa-file sidebar-nav-icon"></i>
                        <span class="sidebar-nav-mini-hide">Reports</span>
                    </a>
                </li>

                <li id="plans_tab">
                    <a href="{{url('mr/plans')}}">
                        <i class="fa fa-calendar sidebar-nav-icon"></i>
                        <span class="sidebar-nav-mini-hide">Plans</span>
                    </a>
                </li>

                <li id="leave_requests_tab">
                    <a href="{{url('mr/leave-requests')}}">
                        <i class="fa fa-sign-out sidebar-nav-icon"></i>
                        <span class="sidebar-nav-mini-hide">Request for leave </span>
                    </a>
                </li>

                <li class="sidebar-header">
                    <span class="sidebar-header-options clearfix"><i class="fa fa-search"></i></span>
                    <span class="sidebar-header-title">Search</span>
                </li>
                <li id="target_search_tab">
                    <a href="{{url('mr/target/search')}}">
                        <i class="fa fa-bar-chart sidebar-nav-icon"></i>
                        <span class="sidebar-nav-mini-hide">Target</span>
                    </a>
                </li>

                <li id="coverage_search_tab">
                    <a href="{{url('mr/coverage/search')}}"><i class="fa fa-pie-chart sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Coverage</span></a>
                </li>
                <li id="report_search_tab">
                    <a href="{{url('mr/reports/search')}}"><i class="fa fa-file sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Reports</span></a>
                </li>
                <li id="plan_search_tab">
                    <a href="{{url('mr/plans/search')}}"><i class="fa fa-calendar sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Plans</span></a>
                </li>

                <li id="leave_requests_search_tab">
                    <a href="{{url('mr/leave-requests/search')}}">
                        <i class="fa fa-sign-out sidebar-nav-icon"></i>
                        <span class="sidebar-nav-mini-hide">Leave Requests</span>
                    </a>
                </li>
            </ul>
            <!-- END Sidebar Navigation -->
            
        </div>
        <!-- END Sidebar Content -->
    </div>
    <!-- END Wrapper for scrolling functionality -->
</div>
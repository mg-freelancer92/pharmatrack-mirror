@extends('mr.layouts.main')

@section('title') Dashboard @endsection
@section('content')
<div id="page-content">
    <!-- Dashboard Header -->
    <!-- For an image header add the class 'content-header-media' and an image as in the following example -->
    <div class="content-header content-header-media">
        <div class="header-section">
            <div class="row">
                <!-- Main Title (hidden on small devices for the statistics to fit) -->
                <div class="col-md-4 col-lg-6 hidden-xs hidden-sm">
                    <h1>Welcome <strong>@if (Auth::check()) {{Auth::user()->name}} @endif</strong><br><small>Look to Today Statistics</small></h1>
                </div>
                <!-- END Main Title -->

                <!-- Top Stats -->
                <div class="col-md-8 col-lg-6">
                    <div class="row text-center">
                        <div class="col-sm-4">
                            <a href="{{url('mr/reports/create')}}">
                                <h2 class="animation-hatch">
                                    <strong><i class="fa fa-file"></i></strong><br>
                                    <small><i class="fa fa-plus"></i> New Report </small>
                                </h2>
                            </a>
                        </div>
                        <div class="col-sm-3">
                            <a href="{{url('mr/plans/create')}}">
                                <h2 class="animation-hatch">
                                    <strong><i class="fa fa-calendar"></i></strong><br>
                                    <small><i class="fa fa-plus"></i> New Plan</small>
                                </h2>
                            </a>
                        </div>
                        <div class="col-sm-5">
                            <a href="{{url('mr/leave-requests/create')}}">
                                <h2 class="animation-hatch">
                                    <strong><i class="fa fa-sign-out"></i></strong><br>
                                    <small><i class="fa fa-plus"></i> New Leave Request</small>
                                </h2>
                            </a>
                        </div>
                    </div>
                </div>
                <!-- END Top Stats -->
            </div>
        </div>
        <!-- For best results use an image with a resolution of 2560x248 pixels (You can also use a blurred image with ratio 10:1 - eg: 1000x100 pixels - it will adjust and look great!) -->
        <img src="{{URL::asset('img/placeholders/headers/dashboard_header.jpg')}}" alt="header image" class="animation-pulseSlow">
    </div>
    <!-- END Dashboard Header -->

    <!-- Mini Top Stats Row -->
    <div class="row">
        <div class="col-md-4">
            <div class="widget-simple widget widget-hover-effect1">
                <div class="widget-icon pull-left themed-background-spring animation-fadeIn">
                    <i class="fa fa-file"></i>
                </div>
                <h3 class="widget-content text-right animation-pullDown">
                    <strong>{{count($reports)}}</strong><br>
                    <small>Reports</small>
                </h3>
            </div>
        </div>

        <div class="col-md-4">
            <div class="widget-simple widget widget-hover-effect1">
                <div class="widget-icon pull-left themed-background-spring animation-fadeIn">
                    <i class="fa fa-pie-chart"></i>
                </div>
                <h3 class="widget-content text-right animation-pullDown">
                    <strong>{{$coveragePercent.'%'}}</strong><br>
                    <small>Coverage</small>
                </h3>
            </div>
        </div>

        <div class="col-md-4">
            <div class="widget-simple widget widget-hover-effect1">
                <div class="widget-icon pull-left themed-background-spring animation-fadeIn">
                    <i class="fa fa-bar-chart"></i>
                </div>
                <h3 class="widget-content text-right animation-pullDown">
                    <strong>29 %</strong><br>
                    <small>Target</small>
                </h3>
            </div>
        </div>
    </div>
    <!-- END Mini Top Stats Row -->

    <div class="row">
        <!-- Input Grid Block -->
        <div class="block">
            <!-- Input Grid Title -->
            <div class="block-title">
                <h2>
                    Quick Stats from
                    <strong>{{$startOfMonth}}</strong>
                    to
                    <strong>{{$endOfMonth}}</strong>
                </h2>
            </div>
            <!-- END Input Grid Title -->

            <div class="block">
                <div class="block-title">
                    <ul class="nav nav-tabs" data-toggle="tabs">
                        <li class="active">
                            <a href="#plan_tab">
                                <i class="fa fa-calendar"></i>
                                Plans
                            </a>
                        </li>
                        <li>
                            <a href="#coverage_tab">
                                <i class="fa fa-pie-chart"></i>
                                Coverage
                            </a>
                        </li>
                        <li>
                            <a href="#report_tab">
                                <i class="fa fa-file"></i>
                                Reports
                            </a>
                        </li>
                        <li>
                            <a href="#target_tab">
                                <i class="fa fa-bar-chart"></i>
                                Target
                            </a>
                        </li>
                    </ul>
                </div>

                <!-- Tabs Content -->
                <div class="tab-content">
                    <div class="tab-pane active" id="plan_tab">
                        <div class="row">
                            <div class="col-md-12">
                                <div id="plans"></div>
                            </div>
                        </div>
                    </div>

                    <div class="tab-pane" id="coverage_tab">
                        <div class="row text-center">
                            <div class="row">
                                <div class="col-sm-6 col-md-offset-3">
                                    <h3>{{$actualCoverage}} From {{$allCoverage}} Visits</h3>
                                    <div class="pie-chart block-section" data-percent="{{$coveragePercent}}" data-size="200">
                                        <span>{{$coveragePercent}}%</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="tab-pane" id="report_tab">
                        <div class="table-responsive">
                            <table class="table table-vcenter table-condensed table-bordered datatable">
                                <thead>
                                <tr>
                                    <th class="text-center">Date</th>
                                    <th class="text-center">Customer</th>
                                    <th class="text-center">Follow Up</th>
                                    <th class="text-center">Sample Products</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($reports as $report)
                                    <tr>
                                        <td class="text-center">{{$report->created_at}}</td>
                                        <td class="text-center">{{$report->customer->name}}</td>
                                        <td class="text-center">{{$report->follow_up}}</td>
                                        <td class="text-center">
                                            <a data-toggle="modal" data-target="#report_{{$report->id}}" title="Product" class="btn btn-xs btn-danger">
                                                <i class="fa fa-list"></i>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>

                            @foreach($reports as $report)
                                <div class="modal fade" id="report_{{$report->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="table-responsive">
                                                <table class="table table-vcenter table-condensed table-bordered datatable">
                                                    <thead>
                                                    <tr>
                                                        <th class="text-center">Name</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @foreach($report->products as $productInReport)
                                                        <tr>
                                                            <td class="text-center">{{$productInReport->product->name}}</td>
                                                        </tr>
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="tab-pane" id="target_tab">

                    </div>
                </div>
                <!-- END Tabs Content -->
            </div>
        </div>
        <!-- END Input Grid Block -->
    </div>
</div>
@endsection

@section('custom_scripts')
    <!-- Load and execute javascript code used only in this page -->
    <script src="{{URL::asset('js/pages/compCalendar.js')}}"></script>
    <script src="{{URL::asset('js/pages/tablesDatatables.js')}}"></script>

    <script>
        $(function(){
            CompCalendar.init();
            TablesDatatables.init();
        });
        $('#dashboard_tab').addClass('active');
        $('.fc-left').css('visibility', 'hidden');
    </script>
    <script>
        // global app configuration object
        var config = {
            routes: [
                { plans: "{{url('mr/plans/ajax') }}"}
            ]
        };
    </script>
    <script>
        /* Initialize FullCalendar */
        var date = new Date();
        var d = date.getDate();
        var m = date.getMonth();
        var y = date.getFullYear();
        $('#plans').fullCalendar({
            header: {
                center: 'title',
            },
            firstDay: 6,
            hiddenDays: [ 5 ],
            events: config.routes[0].plans,
            eventOrder: 'color',
            defaultView : 'month'
        });
    </script>
@endsection
@extends('mr.layouts.main')

@section('title') Add New Report @endsection
@section('content')
<!-- Page content -->
<div id="page-content">
    <!-- Forms General Header -->
    <ul class="breadcrumb breadcrumb-top">
        <li><a href="{{url("mr/dashboard")}}">Dashboard</a></li>
        <li><a href="{{url("mr/reports")}}">Reports</a></li>
        <li><a href="{{url("mr/reports/create")}}">Add New Report</a></li>
    </ul>
    <!-- END Forms General Header -->

    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <!-- Basic Form Elements Block -->
            <div class="block">
                <!-- Basic Form Elements Title -->
                <div class="block-title">
                    <h2><strong>Add New Report</strong></h2>
                </div>
                <!-- END Form Elements Title -->

                <!-- Basic Form Elements Content -->
                <form action="{{url("mr/reports/create")}}" method="post" enctype="multipart/form-data" class="form-horizontal form-bordered">
                    {!! csrf_field() !!}
                    @if(Session::has('message'))
                        <div class="form-group">
                            <div class="alert alert-success alert-dismissable">
                                <i class="fa fa-check"></i>
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                <b> Success : </b> {{ Session::get('message') }}
                            </div>
                        </div>
                    @endif
                    {!! csrf_field() !!}
                    <input type="hidden" name="mr_id" value="@if(Auth::check()) {{Auth::user()->id}} @endif">

                    <div class="form-group">
                        <label class="col-md-2 control-label" >Customer</label>
                        <div class="col-md-10">
                            <select name="customer_id" class="form-control select-chosen">
                                <option value="">Select Customer</option>
                                @foreach($customers as $customer)
                                    <option value="{{$customer->id}}" >{{$customer->name}}</option>
                                @endforeach
                            </select>
                            @if($errors->has('customer_id'))
                                <div class="alert alert-danger">
                                    <i class="fa fa-warning"></i>
                                    <strong>Error :</strong> {{$errors->first('customer_id')}}
                                </div>
                            @endif
                        </div>
                    </div>

                     <div class="form-group">
                        <label class="col-md-2 control-label">Samples Product</label>
                        <div class="col-md-10">
                            <select id="example-select" name="samples_products[]" class="form-control select-chosen" size="1" multiple>
                                <option value="1" >Product1</option>
                                <option value="2" >Product2</option>
                                <option value="3" >Product3</option>
                            </select>
                            @if($errors->has('samples_products'))
                                <div class="alert alert-danger">
                                    <i class="fa fa-warning"></i>
                                    <strong>Error :</strong> {{$errors->first('samples_products')}}
                                </div>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-2 control-label">Feedback</label>
                        <div class="col-md-10">
                            <textarea name="feedback" rows="5" class="form-control">{{old('feedback')}}</textarea>
                            @if($errors->has('feedback'))
                                <div class="alert alert-danger">
                                    <i class="fa fa-warning"></i>
                                    <strong>Error :</strong> {{$errors->first('feedback')}}
                                </div>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-2 control-label">Follow Up</label>
                        <div class="col-md-10">
                            <textarea name="follow_up" rows="5" class="form-control">{{old('follow_up')}}</textarea>
                            @if($errors->has('follow_up'))
                                <div class="alert alert-danger">
                                    <i class="fa fa-warning"></i>
                                    <strong>Error :</strong> {{$errors->first('follow_up')}}
                                </div>
                            @endif
                        </div>
                    </div>

                    <div class="form-group form-actions">
                        <div class="col-md-9 col-md-offset-3">
                            <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-angle-right"></i> Submit</button>
                            <button type="reset" class="btn btn-sm btn-warning"><i class="fa fa-repeat"></i> Reset</button>
                        </div>
                    </div>
                </form>
                <!-- END Basic Form Elements Content -->
            </div>
            <!-- END Basic Form Elements Block -->
        </div>
    </div>
    <!-- END Input Groups Row -->
</div>
<!-- END Page Content -->
@endsection

@section('custom_scripts')
<!-- Load and execute javascript code used only in this page -->
<script src="{{URL::asset('js/pages/formsGeneral.js')}}"></script>
<script>
    $(function(){ FormsGeneral.init(); });
    $('#reports_tab').addClass('active');
</script>
@endsection
@extends('mr.layouts.main')

@section('title') Your Leave Requests @endsection

@section('content')
<!-- Page content -->
<div id="page-content">
    <!-- Calendar Header -->
    <ul class="breadcrumb breadcrumb-top">
        <li><a href="{{url('mr/dashboard')}}">Dashboard</a></li>
        <li><a href="{{url('mr/leave-requests')}}">Leave Requests</a></li>
    </ul>
    <!-- END Calendar Header -->

    <!-- Datatables Content -->
    <div class="block full">
        <div class="block-title">
            <h2><strong> Leave Requests </strong></h2>
            <a href="{{url('mr/leave-requests/create')}}" class="btn btn-success">
                <i class="fa fa-plus"></i>
                Request for Leave
            </a>
        </div>
        <div class="table-responsive">
            <table class="table table-vcenter table-condensed table-bordered datatable">
                <thead>
                <tr>
                    <th class="text-center">Leave Start</th>
                    <th class="text-center">Leave End</th>
                    <th class="text-center">Reason</th>
                </tr>
                </thead>
                <tbody>
                @foreach($leaveRequests as $leaveRequest)
                    <tr>
                        <td class="text-center">{{$leaveRequest->leave_start->format('d-m-Y')}}</td>
                        <td class="text-center">{{$leaveRequest->leave_end->format('d-m-Y')}}</td>
                        <td class="text-center">{{$leaveRequest->reason}}</td>
                    </tr>
                @endforeach

                </tbody>
            </table>
        </div>
    </div>
    <!-- END Datatables Content -->

    <div class="block block-alt-noborder full">
        <div class="row">
            <div class="col-md-12">
                <div id="leaveRequests"></div>
            </div>
        </div>
    </div>
    <!-- END FullCalendar Content -->
</div>
<!-- END Page Content -->
@endsection

@section('custom_scripts')
<!-- Load and execute javascript code used only in this page -->
<script src="{{URL::asset('js/pages/compCalendar.js')}}"></script>
<script src="{{URL::asset('js/pages/tablesDatatables.js')}}"></script>
<script>
    $(function(){
        CompCalendar.init();
        TablesDatatables.init();
    });
    $('#leave_requests_tab').addClass('active');
</script>
<script>
    // global app configuration object
    var config = {
        routes: [
            { leaveRequests : "{{url('mr/leave-requests/ajax') }}"}
        ]
    };
</script>
<script>
    /* Initialize FullCalendar */
    var date = new Date();
    var d = date.getDate();
    var m = date.getMonth();
    var y = date.getFullYear();
    $('#leaveRequests').fullCalendar({
        header: {
            left: 'prev,next',
            center: 'title',
            right: 'month,agendaWeek,agendaDay'
        },
        firstDay: 6,
        hiddenDays: [ 5 ],
        events: config.routes[0].leaveRequests,
        eventOrder: 'color',
        defaultView : 'month',
        timeFormat: ' '
    });
</script>
@endsection
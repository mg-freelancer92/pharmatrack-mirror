@extends('mr.layouts.main')

@section('title') Request for Leave @endsection
@section('content')
<!-- Page content -->
<div id="page-content">
    <!-- Forms General Header -->
    <ul class="breadcrumb breadcrumb-top">
        <li><a href="{{url("mr/dashboard")}}">Dashboard</a></li>
        <li><a href="{{url("mr/leave-requests")}}">Request for Leave</a></li>
    </ul>
    <!-- END Forms General Header -->

    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <!-- Basic Form Elements Block -->
            <div class="block">
                <!-- Basic Form Elements Title -->
                <div class="block-title">
                    <h2><strong>Add New Plan</strong></h2>
                </div>
                <!-- END Form Elements Title -->

                <!-- Basic Form Elements Content -->
                <form action="{{url("mr/leave-requests/create")}}" method="post" enctype="multipart/form-data" class="form-horizontal form-bordered">
                    {!! csrf_field() !!}
                    @if(Session::has('message'))
                        <div class="form-group">
                            <div class="alert alert-success alert-dismissable">
                                <i class="fa fa-check"></i>
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                <b> Success : </b> {{ Session::get('message') }}
                            </div>
                        </div>
                    @endif
                    {!! csrf_field() !!}
                    <input type="hidden" name="mr_id" value="@if(Auth::check()) {{Auth::user()->id}} @endif">
                    <div class="form-group">
                        <label class="col-md-2 control-label">Start Date</label>
                        <div class="col-md-10">
                            <input type="text" name="leave_start" id="example-datepicker" class="form-control input-datepicker" data-date-format="yyyy-mm-dd" placeholder="yyyy-mm-dd"
                            value="{{old('leave_start')}}">
                            @if($errors->has('leave_start'))
                                <div class="alert alert-danger">
                                    <i class="fa fa-warning"></i>
                                    <strong>Error :</strong> {{$errors->first('leave_start')}}
                                </div>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-2 control-label">End Date</label>
                        <div class="col-md-10">
                            <input type="text" name="leave_end" id="example-datepicker" class="form-control input-datepicker" data-date-format="yyyy-mm-dd" placeholder="yyyy-mm-dd"
                                   value="{{old('leave_end')}}">
                            @if($errors->has('leave_end'))
                                <div class="alert alert-danger">
                                    <i class="fa fa-warning"></i>
                                    <strong>Error :</strong> {{$errors->first('leave_end')}}
                                </div>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-2 control-label">Reason</label>
                        <div class="col-md-10">
                            <textarea name="reason" rows="5" class="form-control">{{old('reason')}}</textarea>
                            @if($errors->has('reason'))
                                <div class="alert alert-danger">
                                    <i class="fa fa-warning"></i>
                                    <strong>Error :</strong> {{$errors->first('reason')}}
                                </div>
                            @endif
                        </div>
                    </div>

                    <div class="form-group form-actions">
                        <div class="col-md-9 col-md-offset-3">
                            <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-angle-right"></i> Submit</button>
                            <button type="reset" class="btn btn-sm btn-warning"><i class="fa fa-repeat"></i> Reset</button>
                        </div>
                    </div>
                </form>
                <!-- END Basic Form Elements Content -->
            </div>
            <!-- END Basic Form Elements Block -->
        </div>
    </div>
    <!-- END Input Groups Row -->
</div>
<!-- END Page Content -->
@endsection

@section('custom_scripts')
<!-- Load and execute javascript code used only in this page -->
<script src="{{URL::asset('js/pages/formsGeneral.js')}}"></script>
<script>
    $(function(){ FormsGeneral.init(); });
    $('#leave_requests_tab').addClass('active');
</script>
@endsection
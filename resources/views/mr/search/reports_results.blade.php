@extends('mr.layouts.main')

@section('title') Reports Results @endsection
@section('content')
<!-- Page content -->
<div id="page-content">
    <!-- Datatables Header -->
    <ul class="breadcrumb breadcrumb-top">
        <li><a href="{{url('mr/dashboard')}}">Dashboard</a></li>
        <li><a href="{{url('mr/reports/search')}}">Reports Search</a></li>
    </ul>
    <!-- END Datatables Header -->

    <!-- Datatables Content -->
    <div class="block full">
        <div class="block-title">
            <h2><strong>Reports </strong> Result </h2>
        </div>
        @if(Session::has('message'))
            <div class="form-group">
                <div class="alert alert-success alert-dismissable">
                    <i class="fa fa-check"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <b> Success : </b> {{ Session::get('message') }}
                </div>
            </div>
        @endif

        <div class="table-responsive">
            <table class="table table-vcenter table-condensed table-bordered datatable">
                <thead>
                <tr>
                    <th class="text-center">Date</th>
                    <th class="text-center">Customer</th>
                    <th class="text-center">Follow Up</th>
                    <th class="text-center">Sample Products</th>
                </tr>
                </thead>
                <tbody>
                @foreach($reports as $report)
                <tr>
                    <td class="text-center">{{$report->created_at}}</td>
                    <td class="text-center">{{$report->customer->name}}</td>
                    <td class="text-center">{{$report->follow_up}}</td>
                    <td class="text-center">
                        <a data-toggle="modal" data-target="#report_{{$report->id}}" title="Product" class="btn btn-xs btn-danger">
                            <i class="fa fa-list"></i>
                        </a>
                    </td>
                </tr>
                @endforeach
                </tbody>
            </table>

            @foreach($reports as $report)
            <div class="modal fade" id="report_{{$report->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="table-responsive">
                            <table class="table table-vcenter table-condensed table-bordered datatable">
                                <thead>
                                <tr>
                                    <th class="text-center">Name</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($report->products as $productInReport)
                                    <tr>
                                        <td class="text-center">{{$productInReport->product->name}}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
    <!-- END Datatables Content -->
</div>
<!-- END Page Content -->
@endsection

@section('custom_scripts')
<!-- Load and execute javascript code used only in this page -->
<script src="{{URL::asset('js/pages/tablesDatatables.js')}}"></script>
<script>
    $(function () {
        TablesDatatables.init();
    });
    $('#report_search_tab').addClass('active');
</script>
@endsection
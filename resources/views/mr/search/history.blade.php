@extends('mr.layouts.main')

@section('title') History Search @endsection
@section('content')
    <!-- Page content -->
    <div id="page-content">
        <!-- Forms General Header -->
        <ul class="breadcrumb breadcrumb-top">
            <li><a href="{{url("mr/dashboard")}}">Dashboard</a></li>
            <li><a href="{{url("mr/history/search")}}">Monthly History Search</a></li>
        </ul>
        <!-- END Forms General Header -->

        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <!-- Basic Form Elements Block -->
                <div class="block">
                    <!-- Basic Form Elements Title -->
                    <div class="block-title">
                        <h2><strong>Monthly History Search</strong></h2>
                    </div>
                    <!-- END Form Elements Title -->

                    <!-- Basic Form Elements Content -->
                    <form action="{{url("mr/history/search")}}" method="post" enctype="multipart/form-data" class="form-horizontal form-bordered">
                        {!! csrf_field() !!}
                        @if(Session::has('message'))
                            <div class="form-group">
                                <div class="alert alert-success alert-dismissable">
                                    <i class="fa fa-check"></i>
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                    <b> Success : </b> {{ Session::get('message') }}
                                </div>
                            </div>
                        @endif
                        {!! csrf_field() !!}
                        <div class="form-group">
                            <label class="col-md-3 control-label" >History for Month </label>
                            <div class="col-md-5">
                                <select name="month" id="month" class="form-control select-chosen">
                                    <option value = "">Month</option>
                                    <option value = "01">Jan</option>
                                    <option value = "02">Feb</option>
                                    <option value = "03">Mar</option>
                                    <option value = "04">Apr</option>
                                    <option value = "05">May</option>
                                    <option value = "06">Jun</option>
                                    <option value = "07">Jul</option>
                                    <option value = "08">Aug</option>
                                    <option value = "09">Sep</option>
                                    <option value = "10">Oct</option>
                                    <option value = "11">Nov</option>
                                    <option value = "12">Dec</option>
                                </select>
                                @if($errors->has('month'))
                                    <div class="alert alert-danger">
                                        <i class="fa fa-warning"></i>
                                        <strong>Error :</strong> {{$errors->first('month')}}
                                    </div>
                                @endif
                            </div>

                            <div class="col-md-4">
                                <select name="year" id="year" class="form-control select-chosen">
                                    <option value = "">Year</option>
                                    <option value = "2016">2016</option>
                                    <option value = "2017">2017</option>
                                    <option value = "2018">2018</option>
                                    <option value = "2019">2019</option>
                                    <option value = "2020">2020</option>
                                </select>
                                @if($errors->has('year'))
                                    <div class="alert alert-danger">
                                        <i class="fa fa-warning"></i>
                                        <strong>Error :</strong> {{$errors->first('year')}}
                                    </div>
                                @endif
                            </div>
                        </div>

                        <div class="form-group form-actions">
                            <div class="col-md-9 col-md-offset-3">
                                <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-angle-right"></i> Submit</button>
                                <button type="reset" class="btn btn-sm btn-warning"><i class="fa fa-repeat"></i> Reset</button>
                            </div>
                        </div>
                    </form>
                    <!-- END Basic Form Elements Content -->
                </div>
                <!-- END Basic Form Elements Block -->
            </div>
        </div>
        <!-- END Input Groups Row -->
    </div>
    <!-- END Page Content -->
@endsection

@section('custom_scripts')
    <!-- Load and execute javascript code used only in this page -->
    <script src="{{URL::asset('js/pages/formsGeneral.js')}}"></script>
    <script>
        $('#history_tab').addClass('active');
    </script>
@endsection
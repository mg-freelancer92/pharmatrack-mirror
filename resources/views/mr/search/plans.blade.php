@extends('mr.layouts.main')

@section('title') Plans Search @endsection
@section('content')
    <!-- Page content -->
    <div id="page-content">
        <!-- Forms General Header -->
        <ul class="breadcrumb breadcrumb-top">
            <li><a href="{{url("mr/dashboard")}}">Dashboard</a></li>
            <li><a href="{{url("mr/plans/search")}}">Plans Search</a></li>
        </ul>
        <!-- END Forms General Header -->

        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <!-- Basic Form Elements Block -->
                <div class="block">
                    <!-- Basic Form Elements Title -->
                    <div class="block-title">
                        <h2><strong>Plan Search</strong></h2>
                    </div>
                    <!-- END Form Elements Title -->

                    <!-- Basic Form Elements Content -->
                    <form action="{{url("mr/plans/search")}}" method="post" enctype="multipart/form-data" class="form-horizontal form-bordered">
                        {!! csrf_field() !!}
                        @if(Session::has('message'))
                            <div class="form-group">
                                <div class="alert alert-success alert-dismissable">
                                    <i class="fa fa-check"></i>
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                    <b> Success : </b> {{ Session::get('message') }}
                                </div>
                            </div>
                        @endif
                        {!! csrf_field() !!}
                        <div class="form-group">
                            <label class="col-md-2 control-label" >From</label>
                            <div class="col-md-4">
                                <input type="text" name="from" class="form-control input-datepicker" data-date-format="yyyy-mm-dd" placeholder="yyyy-mm-dd" value="{{old('from')}}">
                                @if($errors->has('from'))
                                    <div class="alert alert-danger">
                                        <i class="fa fa-warning"></i>
                                        <strong>Error :</strong> {{$errors->first('from')}}
                                    </div>
                                @endif
                            </div>

                            <label class="col-md-2 control-label" >To</label>
                            <div class="col-md-4">
                                <input type="text" name="to" class="form-control input-datepicker" data-date-format="yyyy-mm-dd" placeholder="yyyy-mm-dd" value="{{old('to')}}">
                                @if($errors->has('to'))
                                    <div class="alert alert-danger">
                                        <i class="fa fa-warning"></i>
                                        <strong>Error :</strong> {{$errors->first('to')}}
                                    </div>
                                @endif
                            </div>
                        </div>

                        <div class="form-group form-actions">
                            <div class="col-md-9 col-md-offset-3">
                                <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-angle-right"></i> Submit</button>
                                <button type="reset" class="btn btn-sm btn-warning"><i class="fa fa-repeat"></i> Reset</button>
                            </div>
                        </div>
                    </form>
                    <!-- END Basic Form Elements Content -->
                </div>
                <!-- END Basic Form Elements Block -->
            </div>
        </div>
        <!-- END Input Groups Row -->
    </div>
    <!-- END Page Content -->
@endsection

@section('custom_scripts')
    <!-- Load and execute javascript code used only in this page -->
    <script src="{{URL::asset('js/pages/formsGeneral.js')}}"></script>
    <script>
        $(function(){ FormsGeneral.init(); });
        $('#plan_search_tab').addClass('active');
    </script>
@endsection
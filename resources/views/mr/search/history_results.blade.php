@extends('mr.layouts.main')

@section('title') Monthly History Results @endsection
@section('content')
<!-- Page content -->
<div id="page-content">
    <!-- Datatables Header -->
    <ul class="breadcrumb breadcrumb-top">
        <li><a href="{{url('mr/dashboard')}}">Dashboard</a></li>
        <li><a href="{{url('mr/history/search')}}">Monthly History Search</a></li>
    </ul>
    <!-- END Datatables Header -->

    <!-- Datatables Content -->
    <div class="block full">
        <div class="block-title">
            <h2><strong>Leave Requests </strong> Result </h2>
        </div>
        @if(Session::has('message'))
            <div class="form-group">
                <div class="alert alert-success alert-dismissable">
                    <i class="fa fa-check"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <b> Success : </b> {{ Session::get('message') }}
                </div>
            </div>
        @endif

        <div class="row">
            <!-- Input Grid Block -->
            <div class="block">
                <!-- Input Grid Title -->
                <div class="block-title">
                    <h2>
                        Quick Stats from
                        <strong>{{$startOfMonth}}</strong>
                        to
                        <strong>{{$endOfMonth}}</strong>
                    </h2>
                </div>
                <!-- END Input Grid Title -->

                <div class="block">
                    <div class="block-title">
                        <ul class="nav nav-tabs" data-toggle="tabs">
                            <li class="active">
                                <a href="#plan_tab">
                                    <i class="fa fa-calendar"></i>
                                    Plans
                                </a>
                            </li>
                            <li>
                                <a href="#coverage_tab">
                                    <i class="fa fa-pie-chart"></i>
                                    Coverage
                                </a>
                            </li>
                            <li>
                                <a href="#report_tab">
                                    <i class="fa fa-file"></i>
                                    Reports
                                </a>
                            </li>
                            <li>
                                <a href="#target_tab">
                                    <i class="fa fa-bar-chart"></i>
                                    Target
                                </a>
                            </li>
                        </ul>
                    </div>

                    <!-- Tabs Content -->
                    <div class="tab-content">
                        <div class="tab-pane active" id="plan_tab">
                            <div class="row">
                                <div class="col-md-12">
                                    <div id="plans"></div>
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane" id="coverage_tab">
                            <div class="row text-center">
                                <div class="row">
                                    <div class="col-sm-6 col-md-offset-3">
                                        <h3>{{$actualCoverage}} From {{$allCoverage}} Visits</h3>
                                        <div class="pie-chart block-section" data-percent="{{$coveragePercent}}" data-size="200">
                                            <span>{{$coveragePercent}}%</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane" id="report_tab">
                            <div class="table-responsive">
                                <table class="table table-vcenter table-condensed table-bordered datatable">
                                    <thead>
                                    <tr>
                                        <th class="text-center">Date</th>
                                        <th class="text-center">Customer</th>
                                        <th class="text-center">Follow Up</th>
                                        <th class="text-center">Sample Products</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($reports as $report)
                                        <tr>
                                            <td class="text-center">{{$report->created_at}}</td>
                                            <td class="text-center">{{$report->customer->name}}</td>
                                            <td class="text-center">{{$report->follow_up}}</td>
                                            <td class="text-center">
                                                <a data-toggle="modal" data-target="#report_{{$report->id}}" title="Product" class="btn btn-xs btn-danger">
                                                    <i class="fa fa-list"></i>
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>

                                @foreach($reports as $report)
                                    <div class="modal fade" id="report_{{$report->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="table-responsive">
                                                    <table class="table table-vcenter table-condensed table-bordered datatable">
                                                        <thead>
                                                        <tr>
                                                            <th class="text-center">Name</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        @foreach($report->products as $productInReport)
                                                            <tr>
                                                                <td class="text-center">{{$productInReport->product->name}}</td>
                                                            </tr>
                                                        @endforeach
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                        <div class="tab-pane" id="target_tab">

                        </div>
                    </div>
                    <!-- END Tabs Content -->
                </div>
            </div>
            <!-- END Input Grid Block -->
        </div>
    </div>
    <!-- END Datatables Content -->
</div>
<!-- END Page Content -->
@endsection

@section('custom_scripts')
    <!-- Load and execute javascript code used only in this page -->
    <script src="{{URL::asset('js/pages/compCalendar.js')}}"></script>
    <script src="{{URL::asset('js/pages/tablesDatatables.js')}}"></script>

    <script>
        $(function(){
            CompCalendar.init();
            TablesDatatables.init();
        });
        $('#dashboard_tab').addClass('active');
        $('.fc-left').css('visibility', 'hidden');
        $('#history_tab').addClass('active');
    </script>
    <script>
        // global app configuration object
        var config = {
            routes: [
                { plans: "{{url('mr/plans/ajax') }}"}
            ]
        };
    </script>
    <script>
        /* Initialize FullCalendar */
        var date = new Date();
        var d = date.getDate();
        var m = date.getMonth();
        var y = date.getFullYear();
        $('#plans').fullCalendar({
            header: {
                center: 'title',
            },
            firstDay: 6,
            hiddenDays: [ 5 ],
            events: config.routes[0].plans,
            eventOrder: 'color',
            defaultView : 'month'
        });
    </script>
@endsection
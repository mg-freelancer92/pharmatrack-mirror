@extends('mr.layouts.main')

@section('title') Leave Requests Results @endsection
@section('content')
<!-- Page content -->
<div id="page-content">
    <!-- Datatables Header -->
    <ul class="breadcrumb breadcrumb-top">
        <li><a href="{{url('mr/dashboard')}}">Dashboard</a></li>
        <li><a href="{{url('mr/leave-requests/search')}}">Leave Requests Search</a></li>
    </ul>
    <!-- END Datatables Header -->

    <!-- Datatables Content -->
    <div class="block full">
        <div class="block-title">
            <h2><strong>Leave Requests </strong> Result </h2>
        </div>
        @if(Session::has('message'))
            <div class="form-group">
                <div class="alert alert-success alert-dismissable">
                    <i class="fa fa-check"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <b> Success : </b> {{ Session::get('message') }}
                </div>
            </div>
        @endif

        <div class="table-responsive">
            <table class="table table-vcenter table-condensed table-bordered datatable">
                <thead>
                <tr>
                    <th class="text-center">Start Date</th>
                    <th class="text-center">End Date</th>
                    <th class="text-center">Reason</th>
                    <th class="text-center">Approved ? </th>
                </tr>
                </thead>
                <tbody>
                @foreach($leaveRequests as $leaveRequest)
                <tr>
                    <td class="text-center">{{$leaveRequest->leave_start->format('d-m-Y')}}</td>
                    <td class="text-center">{{$leaveRequest->leave_end->format('d-m-Y')}}</td>
                    <td class="text-center">{{$leaveRequest->reason}}</td>
                    <td class="text-center">
                    @if(!isset($leaveRequest->approved))
                        {!! "<span class='label label-info'>Not Reviewed Yet </span>" !!}
                    @elseif($leaveRequest->approved == 0)
                        {!! "<span class='label label-danger'><i class='fa fa-times'></i></span>" !!}
                    @elseif($leaveRequest->approved)
                            {!! "<span class='label label-success'><i class='fa fa-check'></i></span>" !!}
                    @endif
                    </td>
                </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <!-- END Datatables Content -->
</div>
<!-- END Page Content -->
@endsection

@section('custom_scripts')
<!-- Load and execute javascript code used only in this page -->
<script src="{{URL::asset('js/pages/tablesDatatables.js')}}"></script>
<script>
    $(function () {
        TablesDatatables.init();
    });
    $('#leave_requests_search_tab').addClass('active');
</script>
@endsection
@extends('mr.layouts.main')

@section('title') Your Managers @endsection
@section('content')
<!-- Page content -->
<div id="page-content">
    <!-- Datatables Header -->
    <ul class="breadcrumb breadcrumb-top">
        <li>Tables</li>
        <li><a href="">Datatables</a></li>
    </ul>
    <!-- END Datatables Header -->

    <!-- Datatables Content -->
    <div class="block full">
        <div class="block-title">
            <h2><strong>Your </strong> Managers </h2>
        </div>

        <div class="table-responsive">
            <table class="table table-vcenter table-condensed table-bordered datatable">
                <thead>
                <tr>
                    <th class="text-center">Name</th>
                    <th class="text-center">Position</th>
                    <th class="text-center">Email</th>
                    <th class="text-center">Mobile</th>
                </tr>
                </thead>
                <tbody>
                @foreach($managers as $manager)
                <tr>
                    <td class="text-center">{{$manager->name}}</td>
                    <td class="text-center">{{$manager->level->position}}</td>
                    <td class="text-center">{{$manager->email}}</td>
                    <td class="text-center">{{$manager->mobile}}</td>
                </tr>
                @endforeach

                </tbody>
            </table>
        </div>
    </div>
    <!-- END Datatables Content -->
</div>
<!-- END Page Content -->
@endsection

@section('custom_scripts')
<!-- Load and execute javascript code used only in this page -->
<script src="{{URL::asset('js/pages/tablesDatatables.js')}}"></script>
<script>
    $(function () {
        TablesDatatables.init();
    });
    $('#mrs_tab').addClass('active');
</script>
@endsection
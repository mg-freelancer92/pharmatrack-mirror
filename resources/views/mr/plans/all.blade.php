@extends('mr.layouts.main')

@section('title') Your Plans @endsection

@section('content')
<!-- Page content -->
<div id="page-content">
    <!-- Calendar Header -->
    <ul class="breadcrumb breadcrumb-top">
        <li><a href="{{url('sm/dashboard')}}">Dashboard</a></li>
        <li><a href="{{url('mr/plans')}}">Plans</a></li>
    </ul>
    <!-- END Calendar Header -->

    <!-- FullCalendar Content -->
    <div class="block block-alt-noborder full">
        <div class="block-title">
            <h2><strong> Plan Colors</strong></h2>
        </div>
        <div class="row">
            <div class="col-md-3">
                <label class="form-control plan-not-visited">Not Visited Yet</label>
            </div>

            <div class="col-md-3">
                <label class="form-control plan-visited">Visited</label>
            </div>

            <div class="col-md-3">
                <label class="form-control plan-holiday">Holiday</label>
            </div>

            <div class="col-md-3">
                <label class="form-control plan-comment">Comment</label>
            </div>
        </div>
    </div>

    <div class="block block-alt-noborder full">
        <div class="block-title">
            <h2><strong> Plans </strong></h2>
            <a href="{{url('mr/plans/create')}}" class="btn btn-success">
                <i class="fa fa-plus"></i>
                Add New Plan
            </a>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div id="plans"></div>
            </div>
        </div>
    </div>
    <!-- END FullCalendar Content -->
</div>
<!-- END Page Content -->
@endsection

@section('custom_scripts')
<!-- Load and execute javascript code used only in this page -->
<script src="{{URL::asset('js/pages/compCalendar.js')}}"></script>
<script>
    $(function(){ CompCalendar.init(); });
    $('#plans_tab').addClass('active');
</script>
<script>
    // global app configuration object
    var config = {
        routes: [
            { plans: "{{url('mr/plans/ajax') }}"}
        ]
    };
</script>
<script>
    /* Initialize FullCalendar */
    var date = new Date();
    var d = date.getDate();
    var m = date.getMonth();
    var y = date.getFullYear();
    $('#plans').fullCalendar({
        header: {
            left: 'prev,next',
            center: 'title',
            right: 'month,agendaWeek,agendaDay'
        },
        firstDay: 6,
        hiddenDays: [ 5 ],
        events: config.routes[0].plans,
        eventOrder: 'color',
        defaultView : 'month'
    });
</script>
@endsection
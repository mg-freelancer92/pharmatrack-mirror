@extends('sm.layouts.main')

@section('title') Dashboard @endsection
@section('content')
<div id="page-content">
    <!-- Dashboard Header -->
    <!-- For an image header add the class 'content-header-media' and an image as in the following example -->
    <div class="content-header content-header-media">
        <div class="header-section">
            <div class="row">
                <!-- Main Title (hidden on small devices for the statistics to fit) -->
                <div class="col-md-4 col-lg-6 hidden-xs hidden-sm">
                    <h1>Welcome <strong>@if (Auth::check()) {{Auth::user()->name}} @endif</strong><br><small>Look to Today Statistics</small></h1>
                </div>
                <!-- END Main Title -->

                <!-- Top Stats -->
                <div class="col-md-8 col-lg-6">
                    <div class="row text-center">
                        <div class="col-xs-4 col-sm-4">
                            <h2 class="animation-hatch">
                                <strong>930 LE</strong><br>
                                <small><i class="fa fa-money"></i> Sales </small>
                            </h2>
                        </div>
                        <div class="col-xs-4 col-sm-4">
                            <h2 class="animation-hatch">
                                <strong>167</strong><br>
                                <small><i class="fa fa-file"></i> Reports</small>
                            </h2>
                        </div>
                        <div class="col-xs-4 col-sm-4">
                            <h2 class="animation-hatch">
                                <strong>39</strong><br>
                                <small><i class="fa fa-calendar-o"></i> Plans</small>
                            </h2>
                        </div>
                        
                    </div>
                </div>
                <!-- END Top Stats -->
            </div>
        </div>
        <!-- For best results use an image with a resolution of 2560x248 pixels (You can also use a blurred image with ratio 10:1 - eg: 1000x100 pixels - it will adjust and look great!) -->
        <img src="{{URL::asset('img/placeholders/headers/dashboard_header.jpg')}}" alt="header image" class="animation-pulseSlow">
    </div>
    <!-- END Dashboard Header -->

    <!-- Mini Top Stats Row -->
    <div class="row">
        <div class="col-sm-6 col-lg-3">
            <!-- Widget -->
            <a href="page_comp_charts.html" class="widget widget-hover-effect1">
                <div class="widget-simple">
                    <div class="widget-icon pull-left themed-background-fire animation-fadeIn">
                        <i class="fa fa-users"></i>
                    </div>
                    <h3 class="widget-content text-right animation-pullDown">
                        <strong>120</strong><br>
                        <small>Employees</small>
                    </h3>
                </div>
            </a>
            <!-- END Widget -->
        </div>

        <div class="col-sm-6 col-lg-3">
            <!-- Widget -->
            <a href="page_comp_charts.html" class="widget widget-hover-effect1">
                <div class="widget-simple">
                    <div class="widget-icon pull-left themed-background-spring animation-fadeIn">
                        <i class="fa fa-stethoscope"></i>
                    </div>
                    <h3 class="widget-content text-right animation-pullDown">
                        <strong>2500</strong><br>
                        <small>Customers</small>
                    </h3>
                </div>
            </a>
            <!-- END Widget -->
        </div>

        <div class="col-sm-6 col-lg-3">
            <!-- Widget -->
            <a href="page_comp_charts.html" class="widget widget-hover-effect1">
                <div class="widget-simple">
                    <div class="widget-icon pull-left themed-background-autumn animation-fadeIn">
                        <i class="fa fa-medkit"></i>
                    </div>
                    <h3 class="widget-content text-right animation-pullDown">
                        <strong>210</strong><br>
                        <small>Products</small>
                    </h3>
                </div>
            </a>
            <!-- END Widget -->
        </div>

        <div class="col-sm-6 col-lg-3">
            <!-- Widget -->
            <a href="page_comp_charts.html" class="widget widget-hover-effect1">
                <div class="widget-simple">
                    <div class="widget-icon pull-left themed-background-amethyst animation-fadeIn">
                        <i class="fa fa-road"></i>
                    </div>
                    <h3 class="widget-content text-right animation-pullDown">
                        <strong>30</strong><br>
                        <small>Areas</small>
                    </h3>
                </div>
            </a>
            <!-- END Widget -->
        </div>                            
    </div>
    <!-- END Mini Top Stats Row -->
</div>
@endsection

@section('custom_script')
<!-- Load and execute javascript code used only in this page -->
<script src="js/pages/index.js"></script>
<script>$(function(){ Index.init(); });</script>
@endsection
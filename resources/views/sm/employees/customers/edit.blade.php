@extends('sm.layouts.main')

@section('title') {{$customer->name}}@endsection
@section('content')
<!-- Page content -->
<div id="page-content">
    <!-- Forms General Header -->
    <ul class="breadcrumb breadcrumb-top">
        <li><a href="{{url("sm/dashboard")}}">Dashboard</a></li>
        <li><a href="{{url("sm/customers/all")}}">Customers</a></li>
        <li><a href="{{url("sm/customers/add")}}">{{$customer->name}}</a></li>
    </ul>
    <!-- END Forms General Header -->

    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <!-- Basic Form Elements Block -->
            <div class="block">
                <!-- Basic Form Elements Title -->
                <div class="block-title">
                    <h2><strong>{{$customer->name}}</strong></h2>
                </div>
                <!-- END Form Elements Title -->

                <!-- Basic Form Elements Content -->
                <form action="{{url("sm/customers/$customer->id/edit")}}" method="post" enctype="multipart/form-data" class="form-horizontal form-bordered">
                    {!! csrf_field() !!}
                    @if(Session::has('message'))
                        <div class="form-group">
                            <div class="alert alert-success alert-dismissable">
                                <i class="fa fa-check"></i>
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                <b> Success : </b> {{ Session::get('message') }}
                            </div>
                        </div>
                    @endif
                    {!! csrf_field() !!}
                    <input type="hidden" name="id" value="{{$customer->id}}">
                    <div class="form-group">
                        <label class="col-md-2 control-label">Name</label>
                        <div class="col-md-10">
                            <input type="text" name="name" class="form-control" placeholder="Name"
                            value="{{$customer->name}}">
                            @if($errors->has('name'))
                                <div class="alert alert-danger">
                                    <i class="fa fa-warning"></i>
                                    <strong>Error :</strong> {{$errors->first('name')}}
                                </div>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-2 control-label" >Medical Rep</label>
                        <div class="col-md-10">
                            <select id="example-select" name="mr_id" class="form-control select-chosen" size="1">
                                <option value="">Please Select Medical Rep</option>
                                @foreach($MRs as $MR)
                                <option value="{{$MR->id}}" @if($customer->mr_id == $MR->id) selected @endif>
                                    {{$MR->name}}
                                </option>
                                @endforeach
                            </select>
                            @if($errors->has('mr_id'))
                                <div class="alert alert-danger">
                                    <i class="fa fa-warning"></i>
                                    <strong>Error :</strong> {{$errors->first('mr_id')}}
                                </div>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-2 control-label" >Email</label>
                        <div class="col-md-10">
                            <input type="email" name="email" class="form-control" placeholder="Email"
                            value="{{$customer->email}}">
                            @if($errors->has('email'))
                                <div class="alert alert-danger">
                                    <i class="fa fa-warning"></i>
                                    <strong>Error :</strong> {{$errors->first('email')}}
                                </div>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-2 control-label" >Mobile</label>
                        <div class="col-md-10">
                            <input type="text" name="mobile" class="form-control" placeholder="Mobile"
                                   value="{{$customer->mobile}}">
                            @if($errors->has('mobile'))
                                <div class="alert alert-danger">
                                    <i class="fa fa-warning"></i>
                                    <strong>Error :</strong> {{$errors->first('mobile')}}
                                </div>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-2 control-label" >Clinic Phone</label>
                        <div class="col-md-10">
                            <input type="text" name="clinic_phone" class="form-control" placeholder="Mobile"
                                   value="{{$customer->clinic_phone}}">
                            @if($errors->has('clinic_phone'))
                                <div class="alert alert-danger">
                                    <i class="fa fa-warning"></i>
                                    <strong>Error :</strong> {{$errors->first('clinic_phone')}}
                                </div>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-2 control-label">Class</label>
                        <div class="col-md-10">
                            <select id="example-select" name="class" class="form-control select-chosen" size="1">
                                <option value="">Please Select Class</option>
                                <option value="A+" @if($customer->class == 'A+') selected @endif>A+</option>
                                <option value="A" @if($customer->class == 'A') selected @endif>A</option>
                                <option value="B" @if($customer->class == 'B') selected @endif>B</option>
                                <option value="C" @if($customer->class == 'C') selected @endif>C</option>
                                <option value="D" @if($customer->class == 'D') selected @endif>D</option>
                            </select>
                            @if($errors->has('class'))
                                <div class="alert alert-danger">
                                    <i class="fa fa-warning"></i>
                                    <strong>Error :</strong> {{$errors->first('class')}}
                                </div>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-2 control-label">Working Place</label>
                        <div class="col-md-10">
                            <select id="example-select" name="working_place" class="form-control select-chosen" size="1">
                                <option value="">Please Working Place</option>
                                <option value="clinic" @if($customer->working_place == 'clinic') selected @endif >Clinic</option>
                                <option value="polyclinic" @if($customer->working_place == 'polyclinic') selected @endif>Polyclinic</option>
                                <option value="hospital" @if($customer->working_place == 'hospital') selected @endif>Hospital</option>
                                <option value="pharmacy" @if($customer->pharmacy == 'clinic') selected @endif>Pharmacy</option>
                                <option value="medical_center" @if($customer->working_place == 'medical_center') selected @endif>Medical Center</option>
                            </select>
                            @if($errors->has('working_place'))
                                <div class="alert alert-danger">
                                    <i class="fa fa-warning"></i>
                                    <strong>Error :</strong> {{$errors->first('working_place')}}
                                </div>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-2 control-label">Working Place Name</label>
                        <div class="col-md-10">
                            <input type="text" name="working_place_name" class="form-control select-chosen" placeholder="Working Place Name" value="{{$customer->working_place_name}}">

                            @if($errors->has('working_place_name'))
                                <div class="alert alert-danger">
                                    <i class="fa fa-warning"></i>
                                    <strong>Error :</strong> {{$errors->first('working_place_name')}}
                                </div>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-2 control-label">Specialty</label>
                        <div class="col-md-10">
                            <select id="example-select" name="specialty" class="form-control select-chosen" size="1">
                                <option value="">Please Specialty</option>
                                <option value="GYN" @if($customer->specialty == 'GYN') selected @endif>GYN</option>
                                <option value="IM" @if($customer->specialty == 'IM') selected @endif>IM</option>
                                <option value="GP" @if($customer->specialty == 'GP') selected @endif>GP</option>
                                <option value="Surg" @if($customer->specialty == 'Surg') selected @endif>Surg</option>
                                <option value="Orth" @if($customer->specialty == 'Orth') selected @endif>Orth</option>
                                <option value="Ped" @if($customer->specialty == 'Ped') selected @endif>Ped</option>
                                <option value="Ophth" @if($customer->specialty == 'Ophth') selected @endif>Ophth</option>
                                <option value="VS" @if($customer->specialty == 'VS') selected @endif>VS</option>
                            </select>
                            @if($errors->has('specialty'))
                                <div class="alert alert-danger">
                                    <i class="fa fa-warning"></i>
                                    <strong>Error :</strong> {{$errors->first('specialty')}}
                                </div>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-2 control-label">Address</label>
                        <div class="col-md-10">
                            <input type="text" name="address" class="form-control" placeholder="Address" value="{{$customer->address}}">

                            @if($errors->has('address'))
                                <div class="alert alert-danger">
                                    <i class="fa fa-warning"></i>
                                    <strong>Error :</strong> {{$errors->first('address')}}
                                </div>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-2 control-label">Address Description</label>
                        <div class="col-md-10">
                            <input type="text" name="address_description" class="form-control" placeholder="Address Description" value="{{$customer->address_description}}">

                            @if($errors->has('address_description'))
                                <div class="alert alert-danger">
                                    <i class="fa fa-warning"></i>
                                    <strong>Error :</strong> {{$errors->first('address_description')}}
                                </div>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-2 control-label">Working Time</label>
                        <div class="col-md-10">
                            <input type="text" name="working_time" class="form-control" placeholder="Address" value="{{$customer->working_time}}">

                            @if($errors->has('working_time'))
                                <div class="alert alert-danger">
                                    <i class="fa fa-warning"></i>
                                    <strong>Error :</strong> {{$errors->first('working_time')}}
                                </div>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-2 control-label">Time Of Visit</label>
                        <div class="col-md-10">
                            <input type="text" name="time_of_visit" class="form-control" placeholder="Address" value="{{$customer->time_of_visit}}">

                            @if($errors->has('time_of_visit'))
                                <div class="alert alert-danger">
                                    <i class="fa fa-warning"></i>
                                    <strong>Error :</strong> {{$errors->first('time_of_visit')}}
                                </div>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-2 control-label">Comment</label>
                        <div class="col-md-10">
                            <textarea name="comment" rows="5" class="form-control">{{$customer->comment}}</textarea>
                            @if($errors->has('comment'))
                                <div class="alert alert-danger">
                                    <i class="fa fa-warning"></i>
                                    <strong>Error :</strong> {{$errors->first('comment')}}
                                </div>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-2 control-label">Status</label>
                        <div class="col-md-10">
                            <select id="example-select" name="status" class="form-control select-chosen" size="1">
                                <option value="">Please Statue</option>
                                <option value="1" @if($MR->getOriginal('status') == 1) selected @endif>Active</option>
                                <option value="0" @if($MR->getOriginal('status') == 0) selected @endif>Not Active</option>
                            </select>
                            @if($errors->has('status'))
                                <div class="alert alert-danger">
                                    <i class="fa fa-warning"></i>
                                    <strong>Error :</strong> {{$errors->first('status')}}
                                </div>
                            @endif
                        </div>
                    </div>
                    <div class="form-group form-actions">
                        <div class="col-md-9 col-md-offset-3">
                            <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-angle-right"></i> Submit</button>
                            <button type="reset" class="btn btn-sm btn-warning"><i class="fa fa-repeat"></i> Reset</button>
                        </div>
                    </div>
                </form>
                <!-- END Basic Form Elements Content -->
            </div>
            <!-- END Basic Form Elements Block -->
        </div>
    </div>
    <!-- END Input Groups Row -->
</div>
<!-- END Page Content -->
@endsection

@section('custom_scripts')
<!-- Load and execute javascript code used only in this page -->
<script src="{{URL::asset('js/pages/formsGeneral.js')}}"></script>
<script>
    $(function(){ FormsGeneral.init(); });
    $('#customers_tab').addClass('active');
</script>
@endsection
@extends('sm.layouts.main')

@section('title') Add New Customer @endsection
@section('content')
<!-- Page content -->
<div id="page-content">
    <!-- Forms General Header -->
    <ul class="breadcrumb breadcrumb-top">
        <li><a href="{{url("sm/dashboard")}}">Dashboard</a></li>
        <li><a href="{{url("sm/customers/all")}}">Customers</a></li>
        <li><a href="{{url("sm/customers/add")}}">Add New Customer</a></li>
    </ul>
    <!-- END Forms General Header -->

    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <!-- Basic Form Elements Block -->
            <div class="block">
                <!-- Basic Form Elements Title -->
                <div class="block-title">
                    <div class="block-options pull-right">
                        <a href="javascript:void(0)" class="btn btn-alt btn-sm btn-default toggle-bordered enable-tooltip" data-toggle="button" title="Toggles .form-bordered class">No Borders</a>
                    </div>
                    <h2><strong>Add New Customer</strong></h2>
                </div>
                <!-- END Form Elements Title -->

                <!-- Basic Form Elements Content -->
                <form action="{{url("sm/customers/add")}}" method="post" enctype="multipart/form-data" class="form-horizontal form-bordered">
                    {!! csrf_field() !!}
                    @if(Session::has('message'))
                        <div class="form-group">
                            <div class="alert alert-success alert-dismissable">
                                <i class="fa fa-check"></i>
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                <b> Success : </b> {{ Session::get('message') }}
                            </div>
                        </div>
                    @endif
                    {!! csrf_field() !!}
                    <input type="hidden" name="level_id" value="3">
                    <div class="form-group">
                        <label class="col-md-2 control-label">Name</label>
                        <div class="col-md-10">
                            <input type="text" name="name" class="form-control" placeholder="Name"
                            value="{{old('name')}}">
                            @if($errors->has('name'))
                                <div class="alert alert-danger">
                                    <i class="fa fa-warning"></i>
                                    <strong>Error :</strong> {{$errors->first('name')}}
                                </div>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-2 control-label" >Medical Rep</label>
                        <div class="col-md-10">
                            <select id="example-select" name="mr_id" class="form-control select-chosen" size="1">
                                <option value="">Please Select Medical Rep</option>
                                @foreach($MRs as $MR)
                                <option value="{{$MR->id}}" @if (old('mr_id') == $MR->id) selected @endif>{{$MR->name}}</option>
                                @endforeach
                            </select>
                            @if($errors->has('mr_id'))
                                <div class="alert alert-danger">
                                    <i class="fa fa-warning"></i>
                                    <strong>Error :</strong> {{$errors->first('mr_id')}}
                                </div>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-2 control-label" >Email</label>
                        <div class="col-md-10">
                            <input type="email" name="email" class="form-control" placeholder="Email"
                            value="{{old('email')}}">
                            @if($errors->has('email'))
                                <div class="alert alert-danger">
                                    <i class="fa fa-warning"></i>
                                    <strong>Error :</strong> {{$errors->first('email')}}
                                </div>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-2 control-label" >Mobile</label>
                        <div class="col-md-10">
                            <input type="text" name="mobile" class="form-control" placeholder="Mobile"
                                   value="{{old('mobile')}}">
                            @if($errors->has('mobile'))
                                <div class="alert alert-danger">
                                    <i class="fa fa-warning"></i>
                                    <strong>Error :</strong> {{$errors->first('mobile')}}
                                </div>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-2 control-label" >Clinic Phone</label>
                        <div class="col-md-10">
                            <input type="text" name="clinic_phone" class="form-control" placeholder="Clinic Phone"
                                   value="{{old('clinic_phone')}}">
                            @if($errors->has('clinic_phone'))
                                <div class="alert alert-danger">
                                    <i class="fa fa-warning"></i>
                                    <strong>Error :</strong> {{$errors->first('clinic_phone')}}
                                </div>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-2 control-label">Class</label>
                        <div class="col-md-10">
                            <select id="example-select" name="class" class="form-control select-chosen" size="1">
                                <option value="">Please Select Class</option>
                                <option value="A+" @if (old('class') == 'A+') selected @endif>A+</option>
                                <option value="A" @if (old('class') == 'A') selected @endif>A</option>
                                <option value="B" @if (old('class') == 'B') selected @endif>B</option>
                                <option value="C" @if (old('class') == 'C') selected @endif>C</option>
                                <option value="D" @if (old('class') == 'D') selected @endif>D</option>
                            </select>
                            @if($errors->has('class'))
                                <div class="alert alert-danger">
                                    <i class="fa fa-warning"></i>
                                    <strong>Error :</strong> {{$errors->first('class')}}
                                </div>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-2 control-label">Working Place</label>
                        <div class="col-md-10">
                            <select id="example-select" name="working_place" class="form-control select-chosen" size="1">
                                <option value="">Please Working Place</option>
                                <option value="clinic" @if (old('working_place') == 'clinic') selected @endif>Clinic</option>
                                <option value="polyclinic" @if (old('working_place') == 'polyclinic') selected @endif>Polyclinic</option>
                                <option value="hospital" @if (old('working_place') == 'hospital') selected @endif>Hospital</option>
                                <option value="pharmacy" @if (old('working_place') == 'pharmacy') selected @endif>Pharmacy</option>
                                <option value="medical_center" @if (old('working_place') == 'medical_center') selected @endif>Medical Center</option>
                            </select>
                            @if($errors->has('working_place'))
                                <div class="alert alert-danger">
                                    <i class="fa fa-warning"></i>
                                    <strong>Error :</strong> {{$errors->first('working_place')}}
                                </div>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-2 control-label">Working Place Name</label>
                        <div class="col-md-10">
                            <input type="text" name="working_place_name" class="form-control" placeholder="Working Place Name" value="{{old('working_place_name')}}">

                            @if($errors->has('working_place_name'))
                                <div class="alert alert-danger">
                                    <i class="fa fa-warning"></i>
                                    <strong>Error :</strong> {{$errors->first('working_place_name')}}
                                </div>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-2 control-label">Specialty</label>
                        <div class="col-md-10">
                            <select id="example-select" name="specialty" class="form-control select-chosen" size="1">
                                <option value="">Please Specialty</option>
                                <option value="GYN" @if (old('specialty') == 'GYN') selected @endif>GYN</option>
                                <option value="IM" @if (old('specialty') == 'IM') selected @endif>IM</option>
                                <option value="GP" @if (old('specialty') == 'GP') selected @endif>GP</option>
                                <option value="Surg" @if (old('specialty') == 'Surg') selected @endif>Surg</option>
                                <option value="Orth" @if (old('specialty') == 'Orth') selected @endif>Orth</option>
                                <option value="Ped" @if (old('specialty') == 'Ped') selected @endif>Ped</option>
                                <option value="Ophth" @if (old('specialty') == 'Ophth') selected @endif>Ophth</option>
                                <option value="VS" @if (old('specialty') == 'VS') selected @endif>VS</option>
                            </select>
                            @if($errors->has('specialty'))
                                <div class="alert alert-danger">
                                    <i class="fa fa-warning"></i>
                                    <strong>Error :</strong> {{$errors->first('specialty')}}
                                </div>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-2 control-label">Address</label>
                        <div class="col-md-10">
                            <input type="text" name="address" class="form-control" placeholder="Address" value="{{old('address')}}">

                            @if($errors->has('address'))
                                <div class="alert alert-danger">
                                    <i class="fa fa-warning"></i>
                                    <strong>Error :</strong> {{$errors->first('address')}}
                                </div>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-2 control-label">Address Description</label>
                        <div class="col-md-10">
                            <input type="text" name="address_description" class="form-control" placeholder="Address Description" value="{{old('address_description')}}">

                            @if($errors->has('address_description'))
                                <div class="alert alert-danger">
                                    <i class="fa fa-warning"></i>
                                    <strong>Error :</strong> {{$errors->first('address_description')}}
                                </div>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-2 control-label">Working Time</label>
                        <div class="col-md-10">
                            <input type="text" name="working_time" class="form-control" placeholder="Working Time" value="{{old('working_time')}}">

                            @if($errors->has('working_time'))
                                <div class="alert alert-danger">
                                    <i class="fa fa-warning"></i>
                                    <strong>Error :</strong> {{$errors->first('working_time')}}
                                </div>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-2 control-label">Time Of Visit</label>
                        <div class="col-md-10">
                            <input type="text" name="time_of_visit" class="form-control" placeholder="Time Of Visit" value="{{old('time_of_visit')}}">
                            @if($errors->has('time_of_visit'))
                                <div class="alert alert-danger">
                                    <i class="fa fa-warning"></i>
                                    <strong>Error :</strong> {{$errors->first('time_of_visit')}}
                                </div>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-2 control-label">Comment</label>
                        <div class="col-md-10">
                            <textarea name="comment" rows="5" class="form-control">{{old('comment')}}</textarea>
                            @if($errors->has('comment'))
                                <div class="alert alert-danger">
                                    <i class="fa fa-warning"></i>
                                    <strong>Error :</strong> {{$errors->first('comment')}}
                                </div>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-2 control-label">Status</label>
                        <div class="col-md-10">
                            <select id="example-select" name="status" class="form-control select-chosen" size="1">
                                <option value="">Please Status</option>
                                <option value="1" @if (old('status') == '1') selected @endif>Active</option>
                                <option value="0" @if (old('status') == '0') selected @endif>Not Active</option>
                            </select>
                            @if($errors->has('status'))
                                <div class="alert alert-danger">
                                    <i class="fa fa-warning"></i>
                                    <strong>Error :</strong> {{$errors->first('status')}}
                                </div>
                            @endif
                        </div>
                    </div>
                    <div class="form-group form-actions">
                        <div class="col-md-9 col-md-offset-3">
                            <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-angle-right"></i> Submit</button>
                            <button type="reset" class="btn btn-sm btn-warning"><i class="fa fa-repeat"></i> Reset</button>
                        </div>
                    </div>
                </form>
                <!-- END Basic Form Elements Content -->
            </div>
            <!-- END Basic Form Elements Block -->
        </div>
    </div>
    <!-- END Input Groups Row -->
</div>
<!-- END Page Content -->
@endsection

@section('custom_scripts')
<!-- Load and execute javascript code used only in this page -->
<script src="{{URL::asset('js/pages/formsGeneral.js')}}"></script>
<script>
    $(function(){ FormsGeneral.init(); });
    $('#customers_tab').addClass('active');
</script>
@endsection
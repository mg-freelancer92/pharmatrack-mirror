@extends('sm.layouts.main')

@section('title') Leave Requests @endsection

@section('content')
<!-- Page content -->
<div id="page-content">
    <!-- Calendar Header -->
    <ul class="breadcrumb breadcrumb-top">
        <li><a href="{{url('sm/dashboard')}}">Dashboard</a></li>
        <li><a href="{{url('sm/leave-requests')}}">Leave Requests</a></li>
    </ul>
    <!-- END Calendar Header -->

    <!-- Datatables Content -->
    <div class="block full">
        @if(Session::has('message'))
            <div class="form-group">
                <div class="alert alert-{{Session::get('class')}} alert-dismissable">
                    <i class="fa fa-check"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <b> Success : </b> {{ Session::get('message') }}
                </div>
            </div>
        @endif
        <div class="table-responsive">
            <table class="table table-vcenter table-condensed table-bordered datatable">
                <thead>
                <tr>
                    <th class="text-center">Leave Start</th>
                    <th class="text-center">Leave End</th>
                    @foreach($leaveRequests as $leaveRequest)
                        @if(is_null($leaveRequest->approved))
                            <th class="text-center">Actions</th>
                        @else
                            <th class="text-center">Approved ? </th>
                        @endif
                    @endforeach
                </tr>
                </thead>
                <tbody>
                @foreach($leaveRequests as $leaveRequest)
                    <tr>
                        <td class="text-center">{{$leaveRequest->leave_start->format('d-m-Y')}}</td>
                        <td class="text-center">{{$leaveRequest->leave_end->format('d-m-Y')}}</td>
                        @if(is_null($leaveRequest->approved))
                        <td class="text-center">
                            <a href="{{url('sm/leave-requests/approve/'.$leaveRequest->id)}}" class="btn btn-success">
                                <i class="fa fa-check"></i>
                            </a>
                            <a data-toggle="modal" data-target="#leave_request_{{$leaveRequest->id}}" class="btn btn-danger">
                                <i class="fa fa-times"></i>
                            </a>
                        </td>
                        @else
                        <td class="text-center">{!! $leaveRequest->approved
                            ? "<label class='label label-success'> Yes </label>" :
                            "<label class='label label-danger'> No </label>"  !!}
                        </td>
                        @endif
                    </tr>
                    <div class="modal fade" id="leave_request_{{$leaveRequest->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <form action="{{url('sm/leave-requests/decline/'.$leaveRequest->id)}}" method="post" enctype="multipart/form-data" class="form-horizontal form-bordered">
                                    {!! csrf_field() !!}
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title" id="myModalLabel"><i><strong>Decline Reason</strong></i> </h4>
                                    </div>
                                    <div class="modal-body">
                                        <textarea class="form-control" name="reason" rows="5"></textarea>
                                    </div>
                                    <div class="modal-footer">
                                        <button class="btn btn-primary" data-dismiss="modal">No</button>
                                        <button type="submit" class="btn btn-danger">Yes</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                @endforeach

                </tbody>
            </table>
        </div>
    </div>
    <!-- END Datatables Content -->
</div>
<!-- END Page Content -->
@endsection

@section('custom_scripts')
<!-- Load and execute javascript code used only in this page -->
<script src="{{URL::asset('js/pages/compCalendar.js')}}"></script>
<script src="{{URL::asset('js/pages/tablesDatatables.js')}}"></script>
<script>
    $(function(){
        CompCalendar.init();
        TablesDatatables.init();
    });
    $('#leave_requests_tab').addClass('active');
</script>
@endsection
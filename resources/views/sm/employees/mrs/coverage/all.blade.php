@extends('sm.layouts.main')

@section('title') {{$MR->name}} Coverage @endsection
@section('content')
<!-- Page content -->
<div id="page-content">
    <!-- Datatables Header -->
    <ul class="breadcrumb breadcrumb-top">
        <li><a href="{{url('sm/dashboard')}}">Dashboard</a></li>
        <li><a href="#">Medical Rep</a></li>
        <li><a href="#">Coverage</a></li>
    </ul>
    <!-- END Datatables Header -->

    <!-- Easy Pie Charts Block -->
    <div class="block">
        <!-- Easy Pie Charts Title -->
        <div class="block-title">
            <h2>
                <strong>{{$MR->name}}</strong>
                Coverage from <strong>{{$start}} </strong> to <strong>{{$end}}</strong>
            </h2>
        </div>
        <!-- END Easy Pie Charts Title -->

        <!-- Easy Pie Charts Content -->
        <div class="row text-center">
            <div class="row">
                <div class="col-sm-6 col-md-offset-3">
                    <h3>{{$actualCoverage}} From {{$allCoverage}} Visits</h3>
                    <div class="pie-chart block-section" data-percent="{{$coveragePercent}}" data-size="200">
                        <span>{{$coveragePercent}}%</span>
                    </div>
                </div>
            </div>
        </div>
        <!-- END Easy Pie Charts Content -->
    </div>
    <!-- END Easy Pie Charts Block -->

    <!-- Datatables Content -->
    <div class="block full">
        <div class="block-title">
            <h2><strong>All </strong> Customers </h2>
        </div>


        <div class="table-responsive">
            <table class="table table-vcenter table-condensed table-bordered datatable">
                <thead>
                <tr>
                    <th class="text-center">Name</th>
                    <th class="text-center">Medical Rep</th>
                    <th class="text-center">Class</th>
                    <th>Mobile</th>
                    <th>Clinic Phone</th>
                    <th>Working Place</th>
                    <th>Working Place Name</th>
                    <th>Address</th>
                    <th>Visits</th>
                    <th class="text-center">Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach($customers as $customer)
                    <tr>
                        <td class="text-center">{{$customer->name}}</td>
                        <td class="text-center">{{$customer->mr->name}}</td>
                        <td class="text-center">{{$customer->visitClass->name}}</td>
                        <td class="text-center">{{$customer->mobile}}</td>
                        <td class="text-center">{{$customer->clinic_phone}}</td>
                        <td class="text-center">{{$customer->working_place}}</td>
                        <td class="text-center">{{$customer->working_place_name}}</td>
                        <td class="text-center">{{$customer->address}}</td>
                        <td class="text-center">
                            {{$coverageForCustomers[$customer->id]['actual_visits']}}
                            of
                            {{$coverageForCustomers[$customer->id]['total_visits']}}
                        </td>
                        <td class="text-center">
                            <div class="btn-group">
                                <a href="{{url("sm/customers/$customer->id/edit")}}" data-toggle="tooltip" title="Edit" class="btn btn-xs btn-default"><i class="fa fa-pencil"></i></a>
                                <a data-toggle="modal" data-target="#mr_{{$customer->id}}"title="Delete" class="btn btn-xs btn-danger"><i class="fa fa-times"></i></a>
                            </div>
                        </td>
                    </tr>
                    <div class="modal fade" id="mr_{{$customer->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <form action="{{url("sm/customers/$customer->id/delete")}}" method="post" enctype="multipart/form-data" class="form-horizontal form-bordered">
                                    {!! csrf_field() !!}
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title" id="myModalLabel"><i><strong>Confirm the deletion</strong></i> </h4>
                                    </div>
                                    <div class="modal-body">
                                        <h4 class="modal-title" id="myModalLabel">Are sure to delete <strong>{{$customer->name}}</strong> ? </h4>
                                    </div>
                                    <div class="modal-footer">
                                        <button class="btn btn-primary" data-dismiss="modal">No</button>
                                        <button type="submit" class="btn btn-danger">Yes</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                @endforeach

                </tbody>
            </table>
        </div>
    </div>
</div>
<!-- END Page Content -->
@endsection

@section('custom_scripts')
        <!-- Load and execute javascript code used only in this page -->
<script src="{{URL::asset('js/pages/tablesDatatables.js')}}"></script>
<script>
    $(function () {
        TablesDatatables.init();
    });
    $('#mrs_tab').addClass('active');
</script>
@endsection
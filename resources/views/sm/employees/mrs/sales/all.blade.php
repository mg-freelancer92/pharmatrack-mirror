@extends('sm.layouts.main')

@section('title') {{$MR->name}} Sales @endsection
@section('content')
        <!-- Page content -->
<div id="page-content">
    <!-- Datatables Header -->
    <ul class="breadcrumb breadcrumb-top">
        <li><a href="{{url('sm/dashboard')}}">Dashboard</a></li>
        <li><a href="#">Medical Rep</a></li>
        <li><a href="#">Sales</a></li>
    </ul>
    <!-- END Datatables Header -->

    <!-- Easy Pie Charts Block -->
    <div class="block">
        <!-- Easy Pie Charts Title -->
        <div class="block-title">
            <h2>
                <strong>{{$MR->name}}</strong>
                Sales from <strong>{{$start}} </strong> to <strong>{{$end}}</strong>
            </h2>
        </div>
        <!-- END Easy Pie Charts Title -->

        <!-- Easy Pie Charts Content -->
        <div class="row text-center">
            <div class="row">
                <div class="col-sm-6 col-md-offset-3">
                    <div class="pie-chart block-section" data-percent="{{$allSalesPercent}}" data-size="200">
                        <span>{{$allSalesPercent}}%</span>
                    </div>
                </div>
            </div>
        </div>
        <!-- END Easy Pie Charts Content -->
    </div>
    <!-- END Easy Pie Charts Block -->

    <!-- Datatables Content -->
    <div class="block full">
        @if(Session::has('message'))
            <div class="form-group">
                <div class="alert alert-success alert-dismissable">
                    <i class="fa fa-check"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <b> Success : </b> {{ Session::get('message') }}
                </div>
            </div>
        @endif

        <div class="block-title">
            <h2>
                All Products Sales
            </h2>
        </div>

        <div class="table-responsive">
            <table class="table table-vcenter table-condensed table-bordered datatable">
                <thead>
                <tr>
                    <th class="text-center">Name</th>
                    <th class="text-center">Original Target ({{$currentMonth}})</th>
                    <th class="text-center">Actual Sales ({{$currentMonth}})</th>
                    <th class="text-center">Sales Percent</th>
                </tr>
                </thead>
                <tbody>
                @foreach($targets as $target)
                    <tr>
                        <td class="text-center">{{$target->product->name}}</td>
                        <td class="text-center">{{$target->$currentMonth}}</td>
                        <td class="text-center">{{$target->actual_sales}}</td>
                        <td>
                            <div class="pie-chart block-section" data-percent="{{$target->sales_percent}}" data-size="70">
                                <span>{{$target->sales_percent}}%</span>
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
<!-- END Page Content -->
@endsection

@section('custom_scripts')
        <!-- Load and execute javascript code used only in this page -->
<script src="{{URL::asset('js/pages/tablesDatatables.js')}}"></script>
<script>
    $(function () {
        TablesDatatables.init();
    });
    $('#mrs_tab').addClass('active');
</script>
@endsection
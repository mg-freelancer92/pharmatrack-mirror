@extends('sm.layouts.main')

@section('title') {{$MR->name}} Reports @endsection
@section('content')
        <!-- Page content -->
<div id="page-content">
    <!-- Datatables Header -->
    <ul class="breadcrumb breadcrumb-top">
        <li><a href="{{url('sm/dashboard')}}">Dashboard</a></li>
        <li><a href="#">{{$MR->name}}</a></li>
        <li><a href="{{url("sm/mrs/$MR->id/reports")}}">Reports</a></li>
    </ul>
    <!-- END Datatables Header -->

    <!-- Datatables Content -->
    <div class="block full">
        <div class="block-title">
            <h2><strong>All </strong> Reports </h2>
        </div>
        @if(Session::has('message'))
            <div class="form-group">
                <div class="alert alert-success alert-dismissable">
                    <i class="fa fa-check"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <b> Success : </b> {{ Session::get('message') }}
                </div>
            </div>
        @endif

        <div class="table-responsive">
            <table class="table table-vcenter table-condensed table-bordered datatable">
                <thead>
                <tr>
                    <th class="text-center">Date</th>
                    <th class="text-center">Customer</th>
                    <th class="text-center">Total Sold Products Price</th>
                    <th>Feedback</th>
                    <th>Follow Up</th>
                    <th>Double Visits</th>
                    <th>Is Planned</th>
                    <th>Products</th>
                </tr>
                </thead>
                <tbody>
                @foreach($reports as $report)
                    <tr>
                        <td class="text-center">{{$report->created_at->format('d-m-Y')}}</td>
                        <td class="text-center">{{$report->customer->name}}</td>
                        <td class="text-center">{{$report->total_sold_products_price}}</td>
                        <td class="text-center">{{$report->feedback}}</td>
                        <td class="text-center">{{$report->follow_up}}</td>
                        <td class="text-center">{{$report->doubleVisitManagerId->name}}</td>
                        <td class="text-center">{!! $report->is_planned !!}</td>
                        <td class="text-center">
                            <a data-toggle="modal" data-target="#report_{{$report->id}}" title="Delete" class="btn btn-info">
                                <i class="fa fa-list"></i>
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

    @foreach($reports as $report)
    <div class="modal fade" id="report_{{$report->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel"><i><strong>Report Products Details</strong></i> </h4>
                </div>
                <div class="modal-body">
                    <div class="table-responsive">
                        <table class="table table-vcenter table-condensed table-bordered datatable">
                            <thead>
                            <tr>
                                <th class="text-center">Product</th>
                                <th class="text-center">Action</th>
                                <th class="text-center">Price
                                    <i><small>(if sell action is applied)</small></i>
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($report->products as $productInReport)
                            <tr>
                                <td class="text-center">{{$productInReport->product->name}}</td>
                                <td class="text-center">{{ucfirst($productInReport->product_action)}}</td>
                                <td class="text-center">
                                    @if($productInReport->product_action == 'sell')
                                    {{$productInReport->product->price.' L.E'}}
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endforeach
    <!-- END Datatables Content -->
</div>
<!-- END Page Content -->
@endsection

@section('custom_scripts')
<!-- Load and execute javascript code used only in this page -->
<script src="{{URL::asset('js/pages/tablesDatatables.js')}}"></script>
<script>
    $(function () {
        TablesDatatables.init();
    });
    $('#mrs_tab').addClass('active');
</script>
@endsection
@extends('sm.layouts.main')

@section('title') All Medical Reps @endsection
@section('content')
<!-- Page content -->
<div id="page-content">
    <!-- Datatables Header -->
    <ul class="breadcrumb breadcrumb-top">
        <li>Tables</li>
        <li><a href="">Datatables</a></li>
    </ul>
    <!-- END Datatables Header -->

    <!-- Datatables Content -->
    <div class="block full">
        <div class="block-title">
            <h2><strong>All </strong> Medical Reps </h2>
            <a href="{{url('sm/mrs/add')}}" class="btn btn-success">
                <i class="fa fa-plus"></i>
                Add New Medical Rep
            </a>
        </div>
        @if(Session::has('message'))
            <div class="form-group">
                <div class="alert alert-success alert-dismissable">
                    <i class="fa fa-check"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <b> Success : </b> {{ Session::get('message') }}
                </div>
            </div>
        @endif

        <div class="table-responsive">
            <table class="table table-vcenter table-condensed table-bordered datatable">
                <thead>
                <tr>
                    <th class="text-center">Name</th>
                    <th class="text-center">Manager</th>
                    <th>Email</th>
                    <th>Mobile</th>
                    <th>Hiring Date</th>
                    <th>Leaving Date</th>
                    <th>Status</th>
                    <th>Achievements</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach($MRs as $MR)
                <tr>
                    <td class="text-center">{{$MR->name}}</td>
                    <td class="text-center">{{$MR->manager->name}}</td>
                    <td class="text-center">{{$MR->email}}</td>
                    <td class="text-center">{{$MR->mobile}}</td>
                    <td class="text-center">{{$MR->hiring_date->format('d-m-Y')}}</td>
                    <td class="text-center">{{$MR->leaving_date}}</td>
                    <td class="text-center">{!! $MR->status !!}</td>
                    <td class="text-center">
                        <div class="btn-group">
                            <a href="{{url("sm/mrs/$MR->id/plans")}}" title="Plans" class="btn btn-xs btn-info"><i class="fa fa-calendar"></i></a>
                            <a href="{{url("sm/mrs/$MR->id/reports")}}" title="Reports" class="btn btn-xs btn-info"><i class="fa fa-file"></i></a>
                            <a href="{{url("sm/mrs/$MR->id/coverage")}}" title="Coverage" class="btn btn-xs btn-info"><i class="fa fa-pie-chart"></i></a>
                            <a href="{{url("sm/mrs/$MR->id/sales")}}" title="Sales" class="btn btn-xs btn-info"><i class="fa fa-money"></i></a>
                            <a href="#" title="Set Target" class="btn btn-xs btn-danger"><i class="fa fa-plus"></i></a>
                        </div>
                    </td>
                    <td class="text-center">
                        <div class="btn-group">
                            <a href="{{url("sm/mrs/$MR->id/edit")}}" data-toggle="tooltip" title="Edit" class="btn btn-xs btn-default"><i class="fa fa-pencil"></i></a>
                            <a data-toggle="modal" data-target="#mr_{{$MR->id}}"title="Delete" class="btn btn-xs btn-danger"><i class="fa fa-times"></i></a>
                        </div>
                    </td>
                </tr>
                <div class="modal fade" id="mr_{{$MR->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <form action="{{url("sm/mrs/$MR->id/delete")}}" method="post" enctype="multipart/form-data" class="form-horizontal form-bordered">
                            {!! csrf_field() !!}
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="myModalLabel"><i><strong>Confirm the deletion</strong></i> </h4>
                            </div>
                            <div class="modal-body">
                                <h4 class="modal-title" id="myModalLabel">Are sure to delete <strong>{{$MR->name}}</strong> ? </h4>
                            </div>
                            <div class="modal-footer">
                                <button class="btn btn-primary" data-dismiss="modal">No</button>
                                <button type="submit" class="btn btn-danger">Yes</button>
                            </div>
                            </form>
                        </div>
                    </div>
                </div>
                @endforeach

                </tbody>
            </table>
        </div>
    </div>
    <!-- END Datatables Content -->
</div>
<!-- END Page Content -->
@endsection

@section('custom_scripts')
<!-- Load and execute javascript code used only in this page -->
<script src="{{URL::asset('js/pages/tablesDatatables.js')}}"></script>
<script>
    $(function () {
        TablesDatatables.init();
    });
    $('#mrs_tab').addClass('active');
</script>
@endsection
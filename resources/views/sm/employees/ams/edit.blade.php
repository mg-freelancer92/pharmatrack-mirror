@extends('sm.layouts.main')

@section('title') {{$AM->name}} @endsection
@section('content')
<!-- Page content -->
<div id="page-content">
    <!-- Forms General Header -->
    <ul class="breadcrumb breadcrumb-top">
        <li><a href="{{url("sm/dashboard")}}">Dashboard</a></li>
        <li><a href="{{url("sm/ams/all")}}">Area Managers</a></li>
        <li><a href="{{url("sm/ams/$AM->id/edit")}}">{{$AM->name}}</a></li>
    </ul>
    <!-- END Forms General Header -->

    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <!-- Basic Form Elements Block -->
            <div class="block">
                <!-- Basic Form Elements Title -->
                <div class="block-title">
                    <div class="block-options pull-right">
                        <a href="javascript:void(0)" class="btn btn-alt btn-sm btn-default toggle-bordered enable-tooltip" data-toggle="button" title="Toggles .form-bordered class">No Borders</a>
                    </div>
                    <h2><strong>{{$AM->name}}</strong></h2>
                </div>
                <!-- END Form Elements Title -->

                <!-- Basic Form Elements Content -->
                <form action="{{url("sm/ams/$AM->id/edit")}}" method="post" enctype="multipart/form-data" class="form-horizontal form-bordered">
                    @if(Session::has('message'))
                        <div class="form-group">
                            <div class="alert alert-success alert-dismissable">
                                <i class="fa fa-check"></i>
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                <b> Success : </b> {{ Session::get('message') }}
                            </div>
                        </div>
                    @endif

                    {!! csrf_field() !!}

                    <input type="hidden" name="id" value="{{$AM->id}}">
                    <input type="hidden" name="level_id" value="2">
                    <input type="hidden" name="manager_id" value="@if(Auth::check()) {{Auth::user()->id }}@endif">

                    <div class="form-group">
                        <label class="col-md-2 control-label">Name</label>
                        <div class="col-md-10">
                            <input type="text" name="name" class="form-control" placeholder="Name"
                            value="{{$AM->name}}">
                            @if($errors->has('name'))
                                <div class="alert alert-danger">
                                    <i class="fa fa-warning"></i>
                                    <strong>Error :</strong> {{$errors->first('name')}}
                                </div>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-2 control-label" >Email</label>
                        <div class="col-md-10">
                            <input type="email" name="email" class="form-control" placeholder="Enter Email"
                            value="{{$AM->email}}">
                            @if($errors->has('email'))
                                <div class="alert alert-danger">
                                    <i class="fa fa-warning"></i>
                                    <strong>Error :</strong> {{$errors->first('email')}}
                                </div>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-2 control-label" >Mobile</label>
                        <div class="col-md-10">
                            <input type="text" name="mobile" class="form-control" placeholder="Mobile"
                                   value="{{$AM->mobile}}">
                            @if($errors->has('mobile'))
                                <div class="alert alert-danger">
                                    <i class="fa fa-warning"></i>
                                    <strong>Error :</strong> {{$errors->first('mobile')}}
                                </div>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-2 control-label">Hiring Date</label>
                        <div class="col-md-10">
                            <input type="text" id="example-datepicker" name="hiring_date" class="form-control input-datepicker" data-date-format="yyyy-mm-dd" placeholder="dd-mm-yyyy" value="{{$AM->hiring_date->format('Y-m-d')}}">

                            @if($errors->has('hiring_date'))
                                <div class="alert alert-danger">
                                    <i class="fa fa-warning"></i>
                                    <strong>Error :</strong> {{$errors->first('hiring_date')}}
                                </div>
                            @endif
                        </div>
                    </div>

                    @if(isset($AM->leaving_date))
                    <div class="form-group">
                        <label class="col-md-2 control-label">Leaving Date</label>
                        <div class="col-md-10">
                            <input type="text" id="example-datepicker" name="leaving_date" class="form-control input-datepicker" data-date-format="yyyy-mm-dd" placeholder="dd-mm-yyyy" value="{{$AM->leaving_date->format('Y-m-d')}}">
                            @if($errors->has('leaving_date'))
                                <div class="alert alert-danger">
                                    <i class="fa fa-warning"></i>
                                    <strong>Error :</strong> {{$errors->first('leaving_date')}}
                                </div>
                            @endif
                        </div>
                    </div>
                    @endif

                    <div class="form-group">
                        <label class="col-md-2 control-label">Active</label>
                        <div class="col-md-10">
                            <select id="example-select" name="status" class="form-control" size="1">
                                <option value="">Please Statue</option>
                                <option value="1" @if($AM->getOriginal('status') == 1) selected @endif>Active</option>
                                <option value="0" @if($AM->getOriginal('status') == 0) selected @endif>Not Active</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group form-actions">
                        <div class="col-md-9 col-md-offset-3">
                            <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-angle-right"></i> Submit</button>
                            <button type="reset" class="btn btn-sm btn-warning"><i class="fa fa-repeat"></i> Reset</button>
                        </div>
                    </div>
                </form>
                <!-- END Basic Form Elements Content -->
            </div>
            <!-- END Basic Form Elements Block -->
        </div>
    </div>
    <!-- END Input Groups Row -->
</div>
<!-- END Page Content -->
@endsection

@section('custom_scripts')
<!-- Load and execute javascript code used only in this page -->
<script src="{{URL::asset('js/pages/formsGeneral.js')}}"></script>
<script>
    $(function(){ FormsGeneral.init(); });
    $('#ams_tab').addClass('active');
</script>
@endsection
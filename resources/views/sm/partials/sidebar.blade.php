<div id="sidebar">
    <!-- Wrapper for scrolling functionality -->
    <div id="sidebar-scroll">
        <!-- Sidebar Content -->
        <div class="sidebar-content">
            <!-- User Info -->
            <div class="sidebar-section sidebar-user clearfix sidebar-nav-mini-hide">
                <div class="sidebar-user-avatar">
                    <a href="page_ready_user_profile.html">
                        <img src="{{URL::asset('img/placeholders/avatars/avatar2.jpg')}}" alt="avatar">
                    </a>
                </div>
                <div class="sidebar-user-name">Sales Manager</div>
                <div class="sidebar-user-links">
                    <a href="page_ready_user_profile.html" data-toggle="tooltip" data-placement="bottom" title="Profile"><i class="gi gi-user"></i></a>
                    <a href="page_ready_inbox.html" data-toggle="tooltip" data-placement="bottom" title="Messages"><i class="gi gi-envelope"></i></a>
                    <a href="login.html" data-toggle="tooltip" data-placement="bottom" title="Logout"><i class="gi gi-exit"></i></a>
                </div>
            </div>
            <!-- END User Info -->

            <!-- Sidebar Navigation -->
            <ul class="sidebar-nav">
                <li id="dashboard_tab">
                    <a href="{{url('sm/dashboard')}}"><i class="fa fa-tachometer sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Dashboard</span></a>
                </li>
                <li class="sidebar-header">
                    <span class="sidebar-header-options clearfix"><i class="fa fa-users"></i></span>
                    <span class="sidebar-header-title">Basics</span>
                </li>
                <li id="mrs_tab">
                    <a href="{{url('sm/mrs/all')}}"><i class="fa fa-users sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Medical Reps</span></a>
                </li>
                <li id="ams_tab">
                    <a href="{{url('sm/ams/all')}}"><i class="fa fa-users sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Area Managers</span></a>
                </li>
                <li id="customers_tab">
                    <a href="{{url('sm/customers/all')}}"><i class="fa fa-fa fa-stethoscope sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Customers</span></a>
                </li>

                <li id="products_tab">
                    <a href="{{url('sm/products/all')}}">
                        <i class="fa fa-fa fa-medkit sidebar-nav-icon"></i>
                        <span class="sidebar-nav-mini-hide">Products</span>
                    </a>
                </li>

                <li id="areas_tab">
                    <a href="{{url('sm/areas/all')}}">
                        <i class="fa fa-fa fa-road sidebar-nav-icon"></i>
                        <span class="sidebar-nav-mini-hide">Areas</span>
                    </a>
                </li>

                <li class="sidebar-header">
                    <span class="sidebar-header-options clearfix"><i class="fa fa-search"></i></span>
                    <span class="sidebar-header-title">Search</span>
                </li>
                <li id="target_search_tab">
                    <a href="{{url('sm/target/search')}}">
                        <i class="fa fa-bar-chart sidebar-nav-icon"></i>
                        <span class="sidebar-nav-mini-hide">Target</span>
                    </a>
                </li>

                <li id="coverage_search_tab">
                    <a href="{{url('sm/coverage/search')}}"><i class="fa fa-pie-chart sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Coverage</span></a>
                </li>
                <li id="report_search_tab">
                    <a href="{{url('sm/reports/search')}}"><i class="fa fa-file sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Reports</span></a>
                </li>
                <li id="plan_search_tab">
                    <a href="{{url('sm/plans/search')}}"><i class="fa fa-calendar sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Plans</span></a>
                </li>

                <li class="sidebar-header">
                    <span class="sidebar-header-options clearfix"><i class="fa fa-refresh"></i></span>
                    <span class="sidebar-header-title">Updates</span>
                </li>
                <li id="leave_requests_tab">
                    <a href="{{url('sm/leave-requests')}}"><i class="fa fa-sign-out sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Leave Requests</span></a>
                </li>
            </ul>
            <!-- END Sidebar Navigation -->

        </div>
        <!-- END Sidebar Content -->
    </div>
    <!-- END Wrapper for scrolling functionality -->
</div>
<footer class="clearfix">
    <div class="pull-right">
        Powered By <a href="http://mg-freelancer.com" target="_blank">MG Freelancer</a>
    </div>
    <div class="pull-left">
        <span id="year-copy"></span> &copy; <a href="http://goo.gl/TDOSuC" target="_blank">ProUI 3.5</a>
    </div>
</footer>
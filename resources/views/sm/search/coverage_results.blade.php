@extends('sm.layouts.main')

@section('title') Coverage Results @endsection
@section('content')
<!-- Page content -->
<div id="page-content">
    <!-- Datatables Header -->
    <ul class="breadcrumb breadcrumb-top">
        <li><a href="{{url('sm/dashboard')}}">Dashboard</a></li>
        <li><a href="{{url('sm/coverage/search')}}">Coverage Search</a></li>
    </ul>
    <!-- END Datatables Header -->

    <!-- Datatables Content -->
    <div class="block full">
        <div class="block-title">
            <h2><strong>Coverage </strong> Result </h2>
        </div>
        @if(Session::has('message'))
            <div class="form-group">
                <div class="alert alert-success alert-dismissable">
                    <i class="fa fa-check"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <b> Success : </b> {{ Session::get('message') }}
                </div>
            </div>
        @endif

        <div class="table-responsive">
            <table class="table table-vcenter table-condensed table-bordered datatable">
                <thead>
                <tr>
                    <th class="text-center">Medical Rep</th>
                    <th class="text-center">Actual Visits</th>
                    <th class="text-center">All Visits</th>
                    <th class="text-center">Percent</th>
                </tr>
                </thead>
                <tbody>
                @foreach($coverage as $mrCoverage)
                <tr>
                    <td class="text-center">{{$mrCoverage['mr']}}</td>
                    <td class="text-center">{{$mrCoverage['actual']}}</td>
                    <td class="text-center">{{$mrCoverage['all']}}</td>
                    <td class="text-center">
                        <div class="pie-chart block-section" data-percent="{{$mrCoverage['coverage_percent']}}" data-size="75">
                            <span>{{$mrCoverage['coverage_percent']}}%</span>
                        </div>
                    </td>
                </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <!-- END Datatables Content -->
</div>
<!-- END Page Content -->
@endsection

@section('custom_scripts')
<!-- Load and execute javascript code used only in this page -->
<script src="{{URL::asset('js/pages/tablesDatatables.js')}}"></script>
<script>
    $(function () {
        TablesDatatables.init();
    });
    $('#report_search_tab').addClass('active');
</script>
@endsection
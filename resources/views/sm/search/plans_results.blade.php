@extends('sm.layouts.main')

@section('title') Plans Results @endsection
@section('content')
<!-- Page content -->
<div id="page-content">
    <!-- Datatables Header -->
    <ul class="breadcrumb breadcrumb-top">
        <li><a href="{{url('sm/dashboard')}}">Dashboard</a></li>
        <li><a href="{{url('sm/plans/search')}}">Plans Search</a></li>
    </ul>
    <!-- END Datatables Header -->

    <!-- Datatables Content -->
    <div class="block full">
        <div class="block-title">
            <h2><strong>Plans </strong> Result </h2>
        </div>
        @if(Session::has('message'))
            <div class="form-group">
                <div class="alert alert-success alert-dismissable">
                    <i class="fa fa-check"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <b> Success : </b> {{ Session::get('message') }}
                </div>
            </div>
        @endif

        <div class="table-responsive">
            <table class="table table-vcenter table-condensed table-bordered datatable">
                <thead>
                <tr>
                    <th class="text-center">Medical Rep</th>
                    <th class="text-center">Visit Date</th>
                    <th class="text-center">Customers</th>
                </tr>
                </thead>
                <tbody>
                @foreach($plans as $plan)
                <tr>
                    <td class="text-center">{{$plan['mr']}}</td>
                    <td class="text-center">{{$plan['visit_date']->format('d-m-Y')}}</td>
                    <td class="text-center">
                        <a data-toggle="modal" data-target="#plan_{{$plan['id']}}" title="Customers" class="btn btn-xs btn-danger">
                            <i class="fa fa-list"></i>
                        </a>
                    </td>
                </tr>
                @endforeach
                </tbody>
            </table>

            @foreach($plans as $plan)
            <div class="modal fade" id="plan_{{$plan['id']}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="table-responsive">
                            <table class="table table-vcenter table-condensed table-bordered datatable">
                                <thead>
                                <tr>
                                    <th class="text-center">Name</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($plan['customers'] as $customerInPlan)
                                    <tr>
                                        <td class="text-center">{{$customerInPlan->customer->name}}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
    <!-- END Datatables Content -->
</div>
<!-- END Page Content -->
@endsection

@section('custom_scripts')
<!-- Load and execute javascript code used only in this page -->
<script src="{{URL::asset('js/pages/tablesDatatables.js')}}"></script>
<script>
    $(function () {
        TablesDatatables.init();
    });
    $('#plan_search_tab').addClass('active');
</script>
@endsection